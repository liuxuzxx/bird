package org.lx.bird.javacore.constant;

import org.junit.Test;

/**
 * 关于java的枚举类型的实验工作
 * Created by liuxu on 2017-08-29.
 */
public class EnumTest {

    @Test
    public void obtainTypeCode() {
        FileType aClass = FileType.CLASS;
        System.out.println(aClass);
    }

    enum FileType {
        CLASS(1), TXT(2), JAVA(3), SH(4);

        private int typeCode;

        FileType(int typeCode) {
            this.typeCode = typeCode;
        }

        public int getTypeCode() {
            return this.typeCode;
        }
    }
}
