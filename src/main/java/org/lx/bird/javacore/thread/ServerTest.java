package org.lx.bird.javacore.thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 进行线程执行的试验场地
 * Created by liuxu on 17-10-21.
 */
public class ServerTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<String> submit = executorService.submit(new CallTask());
        System.out.println("等待执行完成...");
        String info = submit.get();
        System.out.println(info);
        executorService.shutdown();
    }
}
