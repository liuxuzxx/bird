package org.lx.bird.javacore.thread.workermem;

/**
 * 人的资源控制器
 *
 * Created by liuxu on 2018-01-29.
 */
public class PeopleResourceHandler {

    private volatile PeopleResource people;
    private boolean flag = true;

    public PeopleResourceHandler(){
        people = new PeopleResource();
    }

    public void rename(String name){
        people.setName(name);
        System.out.println(Thread.currentThread()+":"+System.nanoTime()+":设置了name的属性值:"+name);
    }

    public String getName(){
        return people.getName();
    }

    public void setFlag(boolean flag){
        this.flag = flag;
    }

    public boolean getFlag(){
        return flag;
    }

    public void resetResource(){
        people = new PeopleResource();
    }

    public String fetchResource(){
        return people.toString();
    }
}
