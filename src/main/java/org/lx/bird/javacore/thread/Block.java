package org.lx.bird.javacore.thread;

import java.util.concurrent.LinkedBlockingQueue;

import org.junit.Test;

/**
 * 查看阻塞队列的使用方式
 * Created by liuxu on 2017-08-21.
 */
public class Block {

    @Test
    public void testBlock(){
        try {
            LinkedBlockingQueue<String> resources = new LinkedBlockingQueue<>(2);
            resources.add("I");
            resources.add("Have");
            resources.add("a");
            System.out.println("I have a dream.");
        }catch (IllegalStateException e){
            System.out.println("发生阻塞，不过还能恢复");
        }
    }
}
