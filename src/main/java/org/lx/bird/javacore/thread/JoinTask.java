package org.lx.bird.javacore.thread;

/**
 * The join the task order by join to do something.
 * Created by liuxu on 2017-08-03.
 */
public class JoinTask {

    public static void main(String[] args) throws InterruptedException {
        Thread start_1 = new Thread(new ProcessTask("start-1"));
        Thread start_2 = new Thread(new ProcessTask("start-2"));
        Thread start_3 = new Thread(new ProcessTask("start-3"));
        Thread load = new Thread(new ProcessTask("load...", start_1, start_2, start_3));
        Thread end = new Thread(new ProcessTask("end", load));

        end.start();
        end.join();

        System.out.println("end the task...");
    }
}

class ProcessTask implements Runnable {

    private static final long SLEEP_SECOND = 500;

    private String name;

    private Thread[] sonTask;

    private boolean isHaveSon = false;

    public ProcessTask(String name) {
        this.name = name;
    }

    public ProcessTask(String name, Thread... sonTask) {
        this(name);
        this.sonTask = sonTask;
        this.isHaveSon = true;
    }

    @Override
    public void run() {
        sequence();
        for (int index = 0; index < 5; ++index) {
            try {
                Thread.sleep(SLEEP_SECOND);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + ":" + index);
        }
    }

    public void sequence() {
        if (isHaveSon) {
            for (Thread son : sonTask) {
                son.start();
                try {
                    son.join();
                    System.out.println("execute the :" + name);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

