package org.lx.bird.javacore.thread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 测试java原生的自带的并发库给出来的Condition使用效果
 * Created by liuxu on 17-12-17.
 */
public class ConditionWall {

    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        new Thread(new WaterTask(lock, condition)).start();
        new Thread(new WaterTask(lock, condition)).start();
        new Thread(new WaterTask(lock, condition)).start();
        new Thread(new WallTask(lock, condition)).start();
    }
}

class WaterTask implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(WaterTask.class);
    private ReentrantLock lock;
    private Condition condition;

    public WaterTask(ReentrantLock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "---"+System.currentTimeMillis()/1000);
            Thread.sleep(1000);
            condition.await();
            System.out.println("开始执行之后的动作"+System.currentTimeMillis()/1000);
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
        } finally {
            System.out.println(Thread.currentThread().getName() + ":开始释放锁"+System.currentTimeMillis()/1000);
            lock.unlock();
        }
    }
}

class WallTask implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(WallTask.class);

    private ReentrantLock lock;
    private Condition condition;

    public WallTask(ReentrantLock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        lock.lock();
        try {
            System.out.println("开始释放洪水");
            Thread.sleep(1000 * 5);
            condition.signalAll();
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
        } finally {
            lock.unlock();
        }
    }
}
