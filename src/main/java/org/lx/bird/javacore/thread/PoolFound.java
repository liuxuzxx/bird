package org.lx.bird.javacore.thread;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 关于线程池的操作和实验
 * Created by liuxu on 18-5-16.
 */
public class PoolFound {

    /**
     * 测试带有缓存的线程池
     */
    @Test
    public void cachedPool() throws InterruptedException {
        ExecutorService executors = Executors.newCachedThreadPool();
        for (int index = 0; index < 3000; index++) {
            executors.execute(new ForeverTask("线程：" + index));
        }

        while (true) {
            Thread.sleep(1000);
            System.out.println("当前的线程活动数量是：" + ((ThreadPoolExecutor) executors).getActiveCount());
        }
    }

    /**
     * 固定了线程池当中的线程个数
     */
    @Test
    public void fixedPool() throws InterruptedException {
        ExecutorService executors = Executors.newFixedThreadPool(10);
        for (int index = 0; index < 20; index++) {
            executors.execute(new ForeverTask("任务：" + index));
        }

        while (true) {
            Thread.sleep(1000);
            System.out.println("当前的线程活动数量是：" + ((ThreadPoolExecutor) executors).getActiveCount());
        }
    }

    /**
     * 具有周期执行性质的线程池
     */
    @Test
    public void scheduleTask(String[] args) {
        ScheduledExecutorService executors = Executors.newScheduledThreadPool(5);
        executors.schedule(new ForeverTask("循环"), 3, TimeUnit.SECONDS);
    }

    /**
     * 单个线程池子的实验
     */
    @Test
    public void singlePool(){
        ExecutorService single = Executors.newSingleThreadExecutor();
        single.execute(new ForeverTask("单个执行"));
    }

}

/**
 * 实现一个永久执行的任务，然后就是看看线程池的反应
 */
class ForeverTask implements Runnable {

    private String name;

    public ForeverTask(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        Thread.currentThread().setName(name);
        while (true) {
            try {
                System.out.println("查看是否延迟三秒钟执行操作");
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
    }
}
