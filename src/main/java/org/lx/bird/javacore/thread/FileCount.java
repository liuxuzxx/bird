package org.lx.bird.javacore.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * 使用CyclicBarrier进行一个文件系统的某一个文件夹的文件个数的计算
 * 这个的侧重点是在：等待N个完成了，
 * Created by liuxu on 2017-08-03.
 */
public class FileCount {

    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(3,new SumTask());

        Thread home = new Thread(new CountTask(Paths.get("/home"), cyclicBarrier));
        Thread usr = new Thread(new CountTask(Paths.get("/usr"), cyclicBarrier));
        Thread bin = new Thread(new CountTask(Paths.get("/bin"), cyclicBarrier));

        home.start();
        usr.start();
        bin.start();
        System.out.println("Main the count of the file.");
    }

}

class CountTask implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(CountTask.class);
    private Path path;

    private CyclicBarrier cyclicBarrier;

    public CountTask(Path path, CyclicBarrier cyclicBarrier) {
        this.path = path;
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        try {
            System.out.println(Files.list(path).count());
            Thread.sleep(1000);
            System.out.println(Thread.currentThread().getName()+System.currentTimeMillis()/1000);
            cyclicBarrier.await();
            System.out.println("执行完成，进行下一个操作执行动作..." + Thread.currentThread().getName());
        } catch (IOException | InterruptedException | BrokenBarrierException e) {
            logger.error(e.getMessage(),e);
        }
    }
}

class SumTask implements Runnable{
    private static final Logger logger = LoggerFactory.getLogger(SumTask.class);

    @Override
    public void run() {
        try {
            Thread.sleep(1000*5);
        } catch (InterruptedException e) {
            logger.error(e.getMessage(),e);
        }
        System.out.println("执行合并任务...");
    }
}