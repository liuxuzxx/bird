package org.lx.bird.javacore.thread.workermem;

/**
 * 执行多线程的场所
 * Created by liuxu on 2018-01-29.
 */
public class Client {
    public static void main(String[] args){
        PeopleResourceHandler handler = new PeopleResourceHandler();
        Thread police = new Thread(new PoliceStationTask(handler));
        Thread china = new Thread(new ChinaCenterTask(handler));

        china.start();
        police.start();
    }
}
