package org.lx.bird.javacore.thread;

import java.util.Date;

/**
 * 串行执行线程
 * Created by liuxu on 2017-12-15.
 */
public class SequenceThread implements Runnable {

    private LockResource resource;

    public SequenceThread(LockResource resource) {
        this.resource = resource;
    }

    @Override
    public void run() {
        Date now = resource.access();
        System.out.println(Thread.currentThread().getName() + ":" + now.toString());
    }
}
