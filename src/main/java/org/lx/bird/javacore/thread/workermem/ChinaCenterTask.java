package org.lx.bird.javacore.thread.workermem;

/**
 * 国家中心定时任务
 * Created by liuxu on 2018-01-29.
 */
public class ChinaCenterTask implements Runnable{
    private PeopleResourceHandler handler;

    public ChinaCenterTask(PeopleResourceHandler handler) {
        this.handler = handler;
    }

    @Override
    public void run() {
        while (true){
            for(int index=0;index<30;++index) {
                System.out.println(Thread.currentThread()+":"+System.nanoTime()+":"+handler.getName());
            }
            return;
        }
    }
}
