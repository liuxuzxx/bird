package org.lx.bird.javacore.thread;

import java.util.concurrent.Callable;

/**
 * 带有回调性质的多线程编程实验工作
 * Created by liuxu on 17-10-21.
 */
public class CallTask implements Callable<String>{
    @Override
    public String call() throws Exception {
        Thread.sleep(5000);
        return "这是一个可以进行回调的操作...";
    }
}
