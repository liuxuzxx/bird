package org.lx.bird.javacore.thread.workermem;

/**
 * 派出所的任务
 * Created by liuxu on 2018-01-29.
 */
public class PoliceStationTask implements Runnable {

    private PeopleResourceHandler handler;

    public PoliceStationTask(PeopleResourceHandler handler) {
        this.handler = handler;
    }

    @Override
    public void run() {
        while(true){
            for(int index=0;index<30;index++) {
                if(index%10==0) {
                    handler.rename("liuxu"+index);
                }
            }
            return;
        }
    }
}
