package org.lx.bird.javacore.thread.workermem;

import java.util.Date;

/**
 * 人资源的对象
 * Created by liuxu on 2018-01-29.
 */
public class PeopleResource {
    private String id;
    private Date birthday;
    private String address;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
