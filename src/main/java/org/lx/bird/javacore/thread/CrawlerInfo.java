package org.lx.bird.javacore.thread;

import java.util.concurrent.CountDownLatch;

/**
 * crawler the info data.
 * 这个侧重点是，等待多个完成了之后执行另外一个
 * Created by liuxu on 2017-08-03.
 * 网络的人才还真的多啊
 */
public class CrawlerInfo {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);
        for(int index=0;index<5;index++){
            new Thread(new ObtainTask("download:"+index,latch)).start();
        }
        latch.await();
        System.out.println("start handler the html info.");
        Thread.sleep(20000);
        System.out.println("end handler the html info.");
    }
}

class ObtainTask implements Runnable {

    private String name;
    private CountDownLatch latch;

    public ObtainTask(String name, CountDownLatch latch) {
        this.name = name;
        this.latch = latch;
    }

    @Override
    public void run() {
        System.out.println("start download html info data......" + name);
        latch.countDown();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("download end.");
    }
}
