package org.lx.bird.javacore.thread;

import java.util.concurrent.CountDownLatch;

/**
 * 测试CountDownLatch的使用效果
 * Created by liuxu on 17-12-18.
 */
public class RoadCountDown {

    public static void main(String[] args) throws InterruptedException {
        int count = 5;
        CountDownLatch latch = new CountDownLatch(count);
        for (int index = 0; index < count; index++) {
            new Thread(new MaterialTask(latch)).start();
        }
        latch.await();
        System.out.println("开始执行main线程...");
    }

}

class MaterialTask implements Runnable {

    private CountDownLatch latch;

    public MaterialTask(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000 * 3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + "执行完成");
        latch.countDown();
    }
}
