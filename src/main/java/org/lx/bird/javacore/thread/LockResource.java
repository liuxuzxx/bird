package org.lx.bird.javacore.thread;

import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 使用可重入的锁机制。说白了就是从synchronized到lock的一个底层深入计划
 * Created by liuxu on 2017-12-15.
 */
public class LockResource {

    private ReentrantLock lock;

    public LockResource() {
        lock = new ReentrantLock();
    }

    public Date access() {
        try {
            lock.lock();
            Thread.sleep(5*1000);
            System.out.println(Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

        return new Date();
    }
}
