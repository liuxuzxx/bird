package org.lx.bird.javacore.jni;

/**
 * 测试使用java的native的方法调用
 * Create by xu.liu on 2018-03-19
 */
public class NativeC {
    public native int getCount(int number);
    public native String rename(String oldName);

    public static void main(String[] args){
        System.loadLibrary("NativeC'");

        NativeC nativeC = new NativeC();
        String newName = nativeC.rename("old name");
    }
}
