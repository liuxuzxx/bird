package org.lx.bird.javacore.json;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import org.junit.Test;
import org.lx.bird.javacore.dto.AttributesInfo;
import org.lx.bird.javacore.dto.Repository;
import org.lx.bird.javacore.dto.ResponseData;
import org.lx.bird.javacore.dto.SdsFilesInfo;
import org.lx.bird.javacore.dto.UserDto;
import org.lx.bird.javacore.enumtype.ColumnType;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;

import com.alibaba.fastjson.JSONObject;

/**
 * json形式的字符串的使用的场所
 * Created by liuxu on 2017-08-29.
 */
public class JSONString {
    /**
     * 转换一个对象成为一个json字符串,一个attribute的对象
     */
    @Test
    public void convertToString() {
        ArrayList<AttributesInfo> attributesInfos = new ArrayList<>();
        for (int index = 0; index < 3; index++) {
            AttributesInfo attributesInfo = new AttributesInfo();
            attributesInfo.setCode("attr" + index);
            attributesInfo.setValue("value");
            attributesInfos.add(attributesInfo);
        }
        System.out.println(JSONObject.toJSONString(attributesInfos));
    }

    @Test
    public void convertDataFileToString() {
        List<String> raws = new ArrayList<>();
        List<String> records = new ArrayList<>();

        raws.add("raw-1");
        raws.add("raw-2");

        records.add("record-1");
        records.add("record-2");
        records.add("record-3");

        SdsFilesInfo sdsFilesInfo = new SdsFilesInfo();
        sdsFilesInfo.setRawDataFileList(raws);
        sdsFilesInfo.setRecordFileList(records);

        System.out.println(JSONObject.toJSONString(sdsFilesInfo));
    }

    @Test
    public void convertMap() {
        HashMap<String, List<UserDto>> map = new HashMap<>();
        UserDto userDto = new UserDto();
        userDto.setUserName("xi");
        userDto.setBirthday(new Date());
        ArrayList<UserDto> list = new ArrayList<>();
        list.add(userDto);

        ArrayList<UserDto> noList = new ArrayList<>();
        UserDto nono = new UserDto();
        nono.setBirthday(new Date());
        nono.setUserName("nono");
        noList.add(nono);
        map.put("char", list);
        map.put("no", noList);

        System.out.println(JSONObject.toJSONString(map));
    }

    @Test
    public void convertObject() {
        UserDto userDto = new UserDto();
        userDto.setUserName("root");
        userDto.setBirthday(new Date());

        ResponseData responseData = new ResponseData();
        responseData.setData(userDto);
        responseData.setStatus(200);
        String jsonString = JSONObject.toJSONString(responseData);
        System.out.println(jsonString);

        ResponseData data = JSONObject.parseObject(jsonString, ResponseData.class);
        System.out.println((UserDto) data.getData());
    }

    /**
     * 测试enum可以和jsonstring互相转换
     */
    @Test
    public void enumConvert() {
        String jsonString = JSONObject.toJSONString(ColumnType.DATETIME);
        System.out.println(jsonString);
    }

    @Test
    public void nullConvert() {
        UserDto userDto = null;
        String jsonString = JSONObject.toJSONString(userDto);
        System.out.println(jsonString);
        UserDto parseObject = JSONObject.parseObject(jsonString, UserDto.class);
        System.out.println(parseObject == null);
    }

    @Test
    public void interfaceConvert() {
        String path = "/home/user/null";
        Resource resource = new FileSystemResource(path);
        String jsonString = JSONObject.toJSONString(resource);
        System.out.println(jsonString);

        Resource parseObject = JSONObject.parseObject(jsonString, Resource.class);
        System.out.println(parseObject.getClass());
    }

    /**
     * 测试转换isvalid的操作
     */
    @Test
    public void isConvert() {
        Repository repository = new Repository();
        repository.setName("测试转换boolean");
        repository.setIsValid(true);
        System.out.println(JSONObject.toJSONString(repository));
    }

    /**
     * 当属性很多的时候.无论属性多么的不匹配，都是可以转的，只不过有些属性为null而已
     */
    @Test
    public void greaterThan() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "testRepo");
        jsonObject.put("isValid", true);
        jsonObject.put("birth", new Date());

        String json = JSONObject.toJSONString(jsonObject);
        System.out.println(json);
        Repository repository = JSONObject.parseObject(json, Repository.class);
        System.out.println(repository.getName());
    }

    /**
     * 返回插入attributeInfoValue的数据信息
     */
    @Test
    public void mapJson(){
        Map<String, List<String>> dataMap = new HashMap<>();
        dataMap.put("country",Arrays.asList("Japan"));
        dataMap.put("weather",Arrays.asList("Cloudy","Rainy"));
        dataMap.put("frame",Arrays.asList("89"));
        System.out.println(JSONObject.toJSONString(dataMap));
    }
}

