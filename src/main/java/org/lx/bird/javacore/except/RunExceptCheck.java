package org.lx.bird.javacore.except;

/**
 * 抓住运行时的异常
 * Create by xu.liu on 2018-04-13
 */
public class RunExceptCheck {

    public static void main(String[] args){
        for(int index=0;index<3;++index){
            if(index<1){
                throw new RuntimeException("运行时候的异常跑出去");
            }
            System.out.println("index:"+index);
        }
    }
}
