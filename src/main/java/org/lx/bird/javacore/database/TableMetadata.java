package org.lx.bird.javacore.database;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 获取数据库的表的原数据信息，便于，我们生成对应的dto对象
 * Created by liuxu on 7/26/17.
 */
public class TableMetadata {

    public static final String URL = "jdbc:mysql://localhost:3306/rubbish?useUnicode=true&characterEncoding=UTF8";
    public static final String USER_NAME = "root";
    public static final String PASSWORD = "root";
    public static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String DB_TABLE_SCHEMA_INFORMATION_SQL = "SELECT table_name FROM information_schema.TABLES WHERE TABLE_SCHEMA='{0}'";

    @Test
    public void obtainTableMeta() throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        Connection connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM Audition_0");
        ResultSetMetaData metaData = resultSet.getMetaData();

        for (int index = 1; index <= metaData.getColumnCount(); ++index) {
            StringBuilder propertyBuilder = new StringBuilder("private ");
            switch (metaData.getColumnTypeName(index)) {
                case "INT":
                case "TINYINT":
                    propertyBuilder.append("int ");
                    break;
                case "VARCHAR":
                case "CHAR":
                    propertyBuilder.append("String ");
                    break;
                case "DATETIME":
                case "TIMESTAMP":
                    propertyBuilder.append("Date ");
                    break;
                case "DOUBLE":
                    propertyBuilder.append("double ");
                    break;
                default:
                    System.out.println("暂时不支持这个类型，请添加 " + metaData.getColumnTypeName(index));
                    break;
            }
            propertyBuilder.append(parseToCrawler(metaData.getColumnName(index)));
            propertyBuilder.append(";");

            System.out.println(propertyBuilder.toString());
        }
    }

    private String parseToCrawler(String name) {
        String[] split = name.split("_");
        StringBuilder crawler = new StringBuilder();
        crawler.append(split[0]);
        for (int index = 1; index < split.length; ++index) {
            String argu = split[index];
            char c = argu.charAt(0);
            crawler.append(String.valueOf(c).toUpperCase() + argu.substring(1));
        }
        return crawler.toString();
    }

    @Test
    public void initDatabase() throws SQLException {
        List<JSONObject> conditions = conditionJSONObj();

        Connection connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        Statement conditionStatement = connection.createStatement();
        Statement valueStatement = connection.createStatement();
        conditions.forEach(condition -> {
            String conditionCode = condition.get("conditionCode").toString().trim();
            String conditionName = condition.get("conditionName").toString().trim();
            JSONArray values = condition.getJSONArray("conditionValues");
            String conditionSql = "insert into QiTestCondition(code,name,description,isValid,createTime,createBy,updateTime,updateBy)\n" +
                "values('Condition-" + conditionCode + "','" + conditionName + "','',1,now(),'admin',now(),'admin')";

            try {
                conditionStatement.execute(conditionSql);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            values.forEach(value -> {
                String val = value.toString();
                String conditionValuesSql = "insert into QiTestConditionValue(conditionCode,conditionValue,isValid,createTime,createBy,updateTime,updateBy)\n" +
                    "values('Condition-" + conditionCode + "','" + val + "',1,now(),'admin',now(),'admin')";
                try {
                    valueStatement.execute(conditionValuesSql);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        });
        conditionStatement.close();
        valueStatement.close();
    }

    private List<JSONObject> conditionJSONObj() {
        String json = "[" +
            "{" +
            "conditionName: 'iphone'," +
            "conditionCode: '1'," +
            "conditionValues: ['iphone6', 'iphone7', 'iphone8', 'iphoneX']" +
            "}," +
            "{" +
            "conditionName: 'Toolkit'," +
            "conditionCode: '2'," +
            "conditionValues: ['version']" +
            "}," +
            "{" +
            "conditionName: 'Classification'," +
            "conditionCode: '3'," +
            "conditionValues: ['Freeway', 'Arterial road', 'Connector road', 'Local road']" +
            "}," +
            "{" +
            "conditionName: 'Grade separation'," +
            "conditionCode: '4'," +
            "conditionValues: ['True', 'False']" +
            "}," +
            "{" +
            "conditionName: 'Intersection'," +
            "conditionCode: '5'," +
            "conditionValues: ['True', 'False']" +
            "}," +
            "{" +
            "conditionName: 'Traffic marking type'," +
            "conditionCode: '6'," +
            "conditionValues: ['Single solid', 'Single dash', 'Mixure']" +
            "}," +
            "{" +
            "conditionName: 'Pavement'," +
            "conditionCode: '7'," +
            "conditionValues: ['asphalt', 'cement concrete']" +
            "}," +
            "{" +
            "conditionName: 'Signs type'," +
            "conditionCode: '8'," +
            "conditionValues: ['Warning Signs', 'Regulatory Signs', 'Informative Signs']" +
            "}," +
            "{" +
            "conditionName: 'Weather'," +
            "conditionCode: '9'," +
            "conditionValues: ['Cloudy', 'Rainy', 'Snow']" +
            "}," +
            "{" +
            "conditionName: 'Lane Distance'," +
            "conditionCode: '10'," +
            "conditionValues: ['>2500meters', '<2500meters']" +
            "}," +
            "{" +
            "conditionName: 'The number of record times about same road'," +
            "conditionCode: '11'," +
            "conditionValues: ['Each lane once', 'Each lane twice']" +
            "}," +
            "{" +
            "conditionName: 'Scope'," +
            "conditionCode: '12'," +
            "conditionValues: ['road feature', 'traffic signs', 'external environment', 'vehicle behavior', 'sensor itself']" +
            "}," +
            "{" +
            "conditionName: 'option'," +
            "conditionCode: '13'," +
            "conditionValues: ['key test case', 'regular test cases']" +
            "}," +
            "{" +
            "conditionName: 'Level'," +
            "conditionCode: '14'," +
            "conditionValues: ['Low', 'Medium', 'High']" +
            "}," +
            "{" +
            "conditionName: 'The starting position of each record'," +
            "conditionCode: '15'," +
            "conditionValues: ['Identical', 'Distinctive']" +
            "}," +
            "{" +
            "conditionName: 'Lane Count'," +
            "conditionCode: '16'," +
            "conditionValues: ['Two lanes', 'Four lanes', 'Six lanes']" +
            "}," +
            "{" +
            "conditionName: 'Country'," +
            "conditionCode: '17'," +
            "conditionValues: ['China', 'Germany', 'Japan', 'UK', 'US']" +
            "}," +
            "{" +
            "conditionName: 'Traffic density'," +
            "conditionCode: '18'," +
            "conditionValues: ['All']" +
            "}," +
            "{" +
            "conditionName: 'Surrounding land cover type'," +
            "conditionCode: '19'," +
            "conditionValues: ['building', 'forest']" +
            "}," +
            "{" +
            "conditionName: 'Traffic marking color'," +
            "conditionCode: '20'," +
            "conditionValues: ['red', 'yellow', 'white', 'all']" +
            "}," +
            "{" +
            "conditionName: 'Tunnel'," +
            "conditionCode: '21'," +
            "conditionValues: ['True', 'False']" +
            "}," +
            "{" +
            "conditionName: 'Bridge'," +
            "conditionCode: '22'," +
            "conditionValues: ['True', 'False']" +
            "}," +
            "{" +
            "conditionName: 'VTD'," +
            "conditionCode: '23'," +
            "conditionValues: ['Version:Ver0.1,Camera FOV:65,Resolution:1280*800,GPS Frequency:1Hz,IMU Frequency:100Hz']" +
            "}," +
            "{" +
            "conditionName: 'A-sample'," +
            "conditionCode: '24'," +
            "conditionValues: ['Version:Ver0.1,Camera FOV:65,Resolution:1280*800,GPS Frequency:1Hz,IMU Frequency:100Hz']" +
            "}," +
            "{" +
            "conditionName: 'Interchange'," +
            "conditionCode: '25'," +
            "conditionValues: ['Include', 'Exclude']" +
            "}" +
            "]";

        JSONArray objects = JSONObject.parseArray(json);
        List<JSONObject> jsonObjects = objects.toJavaList(JSONObject.class);
        return jsonObjects;
    }

    @Test
    public void dbSchemaInformation() throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        Connection connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(MessageFormat.format(DB_TABLE_SCHEMA_INFORMATION_SQL, "sds_d_0"));
        Set<String> tableNames = new HashSet<>();

        while (resultSet.next()) {
            String tableName = resultSet.getString("table_name");
            String originalTableName = tableName.substring(0, tableName.length() - 2);
            tableNames.add(originalTableName);
        }

        tableNames.forEach(tableName -> {
            String indexSql = "ALTER TABLE " + tableName + "_{number} ADD INDEX IK_" + tableName + "_{number}_isValid(`isValid`);";
            System.out.println(indexSql);
        });
    }

    @Test
    public void compareMainDb() throws ClassNotFoundException, SQLException {
        String devUrl = "jdbc:mysql://10.74.29.158:3306/sds?useUnicode=true&characterEncoding=UTF8";
        String devUserName = "sds";
        String devPassword = "sds1234";
        Class.forName(DRIVER);
        Connection devConnection = DriverManager.getConnection(devUrl, devUserName, devPassword);
        Statement devStatement = devConnection.createStatement();
        String devSql = "SELECT table_name FROM information_schema.TABLES WHERE TABLE_SCHEMA='sds'";
        ResultSet devResultSet = devStatement.executeQuery(devSql);
        Set<String> devTableNames = fetchTableNames(devResultSet);

        String stgUrl = "jdbc:mysql://10.74.29.228:3306/sds_0?useUnicode=true&characterEncoding=UTF8";
        String stgUserName = "sds_stg1";
        String stgPassword = "sds1234";
        Class.forName(DRIVER);
        Connection stgConnection = DriverManager.getConnection(stgUrl, stgUserName, stgPassword);
        Statement stgStatement = stgConnection.createStatement();
        String stgSql = "SELECT table_name FROM information_schema.TABLES WHERE TABLE_SCHEMA='sds_0'";
        ResultSet stgResultSet = stgStatement.executeQuery(stgSql);
        Set<String> stgTableNames = fetchTableNames(stgResultSet);

        boolean flag = devTableNames.removeAll(stgTableNames);
        devTableNames.forEach(tableName -> {
            System.out.println(tableName);
        });
    }

    private Set<String> fetchTableNames(ResultSet resultSet) throws SQLException {
        Set<String> tableNames = new HashSet<>();
        while (resultSet.next()) {
            String tableName = resultSet.getString("table_name");
            if (tableName.startsWith("QRTZ_") || tableName.startsWith("DATABASE")) {

            } else {
                tableName = tableName.substring(0, tableName.length() - 2);
                tableNames.add(tableName);
            }
        }
        return tableNames;
    }
}
