package org.lx.bird.javacore.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JDBC的原始的数据库事务的实验
 * Create by xu.liu on 2018-05-10
 */
public class TransactionFound {
    private static final Logger logger = LoggerFactory.getLogger(TransactionFound.class);

    @Test
    public void transaction() throws ClassNotFoundException, SQLException {
        Class.forName(TableMetadata.DRIVER);
        Connection connection = DriverManager.getConnection(TableMetadata.URL, TableMetadata.USER_NAME, TableMetadata.PASSWORD);
        connection.setAutoCommit(false);
        Statement statement = connection.createStatement();
        try {
            boolean flag = statement.execute("delete from RecordAttributeInfo where categoryCode='weather'");
            throw new SQLException("执行错误");
        } catch (SQLException ex) {
            logger.error(ex.getMessage(),ex);
            connection.rollback();
        }finally {
            connection.commit();
            statement.close();
            connection.close();
        }
    }
}
