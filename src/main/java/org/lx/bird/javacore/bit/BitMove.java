package org.lx.bird.javacore.bit;

import org.junit.Test;

/**
 * java的bit操作的测试
 * Created by liuxu on 2017-08-17.
 */
public class BitMove {
    @Test
    public void testCheck2Power() {
        System.out.println(check2Power(12));
        System.out.println(check2Power(16));
        System.out.println(check2Power(1));
        System.out.println(check2Power(0));
        System.out.println(check2Power(1 << 31));
        System.out.println(check2Power(1 << 30));
        System.out.println(check2Power(1 << 31));
        System.out.println(1 << 31);
    }

    /**
     * 检测一个数字是否是2的幂
     *
     * @param number 查询数字
     * @return true：是 false：不是
     */
    private boolean check2Power(int number) {
        if (number < 1) {
            return false;
        }
        return (number & number - 1) == 0;
    }
}
