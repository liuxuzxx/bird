package org.lx.bird.javacore.basictype;

import org.junit.Test;

/**
 * 转换Double类型的操作
 * Created by liuxu on 2017-10-25.
 */
public class DoubleConvert {
    @Test
    public void convert(){
        String number = "12";
        try {
            double parse = Double.parseDouble(number);
            System.out.println("parse:"+parse);
        }catch (NumberFormatException ex){
            System.out.println("转换错误，不是一个double数字");
        }
    }

}
