package org.lx.bird.javacore.basictype;

import java.time.LocalDate;

import org.junit.Test;

/**
 * 时间的解析实验工作
 * Created by liuxu on 2018-01-23.
 */
public class TimeParser {

    @Test
    public void parseAnonTime(){
        String dateStr = "2001-11-15";
        LocalDate date = LocalDate.parse(dateStr);
    }
}
