package org.lx.bird.javacore.basictype;

import org.junit.Test;

/**
 * 测试数组的类型是什么
 * Create by xu.liu on 2018-05-07
 */
public class ArrType {

    @Test
    public void arr(){
        String[] strings = new String[10];
        Class<? extends String[]> aClass = strings.getClass();
        System.out.println(strings.getClass());
    }
}
