package org.lx.bird.javacore.basictype;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 测试springframework框架自己生成的密码
 * Create by xu.liu on 2018-03-21
 */
public class BCryPassword {

    @Test
    public void password(){
        String original = "test1234";
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode(original);
        System.out.println(encode);
    }
}
