package org.lx.bird.javacore.generic;

import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.lx.bird.javacore.dto.UserDto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 测试list的转泛型的操作
 * Created by liuxu on 17-9-6.
 */
public class ListToEntity {

    /**
     * 测试list的集合进行一个泛型转变操作
     */
    @Test
    public void listConvertToEntity() {
        ResultConvertEntity convert = new ResultConvertEntity();
        List<Object> entity = convert.<UserDto>convert();
        System.out.println(JSONObject.toJSONString((UserDto)entity.get(0)));
    }

}

interface ConvertEntity{
    <E> List<E> convert();
}

class ResultConvertEntity implements ConvertEntity{

    @Override
    public List<Object> convert() {
        List<Object> objects = new ArrayList<>();
        for (int index = 0; index < 10; index++) {
            UserDto userDto = new UserDto();
            userDto.setUserId(index);
            userDto.setBirthday(new Date());
            userDto.setUserName("index:" + index);
            objects.add(userDto);
        }
        return objects;
    }
}
