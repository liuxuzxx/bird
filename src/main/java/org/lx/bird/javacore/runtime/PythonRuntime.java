package org.lx.bird.javacore.runtime;

import org.junit.Test;

import java.io.IOException;
import java.net.URL;

/**
 * 测试使用Runtime调用python脚本
 * Created by liuxu on 17-10-28.
 */
public class PythonRuntime {

    @Test
    public void loadPython() throws IOException {
        String[] execArgs = new String[]{};
        Process process = Runtime.getRuntime().exec(execArgs);
        URL resource = this.getClass().getResource("/spring.xml");
        String path = resource.getPath();
        System.out.println(path);
    }
}
