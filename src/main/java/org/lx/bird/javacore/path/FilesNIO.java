package org.lx.bird.javacore.path;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * The test of the files nio operation
 * Created by liuxu on 7/17/17.
 */
public class FilesNIO {

    private static Path source = Paths.get("/home/user/Documents", "ubuntu-14.04.5-desktop-amd64.iso");
    private static Path copy = Paths.get("/home/user", "test.iso");

    @Test
    public void copyNIO() throws IOException {
        Files.copy(source, Paths.get("/home/user", "test.iso"));
        System.out.println("Test the file nio stream.");
    }

    @Test
    public void newInputStream() throws IOException {
        try (InputStream in = Files.newInputStream(source, StandardOpenOption.READ)) {
            int count = in.available();
            System.out.println(count);
        }
    }

    public static void main(String[] args) {
        Path path = Paths.get("/home/user", "test.test");
        new Thread(() -> {
            Thread.currentThread().setName("show the file handler");
            /*try (InputStream in = Files.newInputStream(source, StandardOpenOption.READ)) {
                int count = in.available();
                System.out.println(count);
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            try {
                InputStream testIn = Files.newInputStream(copy, StandardOpenOption.CREATE_NEW);
                testIn.reset();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("close the file handler");
        }).start();
    }
}
