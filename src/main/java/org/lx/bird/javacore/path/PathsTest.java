package org.lx.bird.javacore.path;

import java.nio.file.Paths;

import org.junit.Test;

/**
 * 测试java8的Paths的工具类
 * Create by xu.liu on 2018-03-09
 */
public class PathsTest {

    @Test
    public void createPath(){
        String pathStr = Paths.get("/data/sds-data/tmp", "raw", "sdss-89/", "sds90").toString();
        System.out.println(pathStr);
    }
}
