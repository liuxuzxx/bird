package org.lx.bird.javacore.path;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * gain the this class Path
 * Created by liuxu on 7/10/17.
 */
public class ThisClassPath {

    @Test
    public void getThisPath() {
        URL resource = this.getClass().getResource("");
        System.out.println(resource.getPath());

        URL resource1 = this.getClass().getClassLoader().getResource("");
        System.out.println(resource1.getPath());

        URL systemResource = ClassLoader.getSystemResource("");
        System.out.println(systemResource.getPath());
    }

    @Test
    public void getStream() {
        String json = "tag.json";
        //readResources(this.getClass().getResourceAsStream(json));
        //readResources(ThisClassPath.class.getClassLoader().getResourceAsStream(json));
        readResources(ClassLoader.getSystemResourceAsStream(json));
    }

    private void readResources(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        Stream<String> lines = bufferedReader.lines();
        String collect = lines.collect(Collectors.joining());
        System.out.println(collect);
    }
}
