package org.lx.bird.javacore.string;

import org.junit.Test;

/**
 * 格式化字符串的测试
 * Created by liuxu on 2017-09-25.
 */
public class FormatString {

    @Test
    public void numberFormat(){
        String code = String.format("SDS-%d", 900);
        System.out.println(code);
    }
}
