package org.lx.bird.javacore.string;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;

/**
 * 使用java生成模板形式的ssh访问的curl
 * Create by xu.liu on 2018-04-22
 */
public class SshTemplate {

    @Test
    public void notifyA1FilePrepare() throws IOException {
        List<String> codes = Files.readAllLines(Paths.get("/home/user/cccc.1"));

        StringBuilder builder = new StringBuilder();
        codes.forEach(code -> {
            builder.append("curl http://localhost:12306/record/" + code + "/a0-anonymous/notify -X PUT\n");
        });

        Files.write(Paths.get("/home/user/notify.sh"),builder.toString().getBytes());
    }
}
