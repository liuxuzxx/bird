package org.lx.bird.javacore.string;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.lx.bird.javacore.utils.FileUtils;

/**
 * 拼凑sql的小工具
 * Created by liuxu on 2018-02-03.
 */
public class SQLString {

    @Test
    public void addColumn() throws IOException {
        StringBuilder builder = new StringBuilder("");
        for (int x = 0; x < 16; x++) {
            builder.append("use sds_" + x + ";\n");
            for (int index = 0; index < 16; index++) {
                builder.append("ALTER TABLE SdsRecord_" + index + " ADD COLUMN recordName VARCHAR(200) NOT NULL DEFAULT '';\n");
            }
        }
        FileUtils.writeAll("/home/user/2018-2-3.sql", builder.toString());
        System.out.println(builder.toString());
    }

    @Test
    public void categorySql() {
        String[] categories = new String[]{"Graphic Pavement Markings",
                "Longitudinal Pavement Markings", "Weather", "Light condition", "LightDirection",
                "Road", "Country", "Misc features and objects"};
        List<String> categoryList = Arrays.asList(categories);

        StringBuilder builder = new StringBuilder();

        categoryList.forEach(category -> {
            builder.append("INSERT INTO RecordAttributeCategoryInfo(categoryCode, categoryName, createTime, createBy, updateBy, isValid, updateTime)\n" +
                    "    VALUES(");
            builder.append("'" + category.trim().replaceAll(" ", "_").toLowerCase() + "',");
            builder.append("'" + category.trim() + "',");
            builder.append("now(),'admin','admin',1,now()");
            builder.append(");\n");
        });

        System.out.println(builder.toString());
    }

    @Test
    public void attributeSql() {
        Map<String, List<String>> dataMap = new HashMap<>();
        dataMap.put("Graphic Pavement Markings", Arrays.asList("GraphicMarkings-RTV", "NonMarkings-RTV"));
        dataMap.put("Longitudinal Pavement Markings", Arrays.asList("LineMarkingsPattern-RTV", "LineMarkingsColor-RTV"));
        dataMap.put("Weather", Arrays.asList("Weather-RTV"));
        dataMap.put("Light condition", Arrays.asList("Illumination-RTV"));
        dataMap.put("LightDirection", Arrays.asList("LightDirection-RTV"));
        dataMap.put("Road", Arrays.asList("IntersectType-RTV", "RoadType-RTV", "LaneCount-RTV"));
        dataMap.put("Country", Arrays.asList("Country-RTV"));
        dataMap.put("Misc features and objects", Arrays.asList("OtherFeatures-RTV"));

        StringBuilder builder = new StringBuilder();
        dataMap.forEach((key, values) -> values.forEach(attr -> {
            builder.append("INSERT INTO RecordAttributeInfo(categoryCode,attrCode, attrName, attrDesc, attrType, attrValueType, isValid, writable, createTime, createBy, updateTime, updateBy)\n" +
                    "    VALUES (");
            builder.append("'" + key.trim().replaceAll(" ", "_").toLowerCase() + "',");
            builder.append("'" + attr.trim().replaceAll(" ", "_").replaceAll("-", "_").toLowerCase() + "',");
            builder.append("'" + attr.trim() + "',");
            builder.append("'',1,0,1,1,now(),'admin',now(),'admin'");
            builder.append(");\n");
        }));
        System.out.println(builder.toString());
    }

    @Test
    public void attributeValueSql() {
        Map<String, List<String>> dataMap = new HashMap<>();
        dataMap.put("GraphicMarkings-RTV ", Arrays.asList("Stop line", "Pedestrian crossing",
                "Arrow", "Speed limit", "Character", "Prohibition and exclusion markings"));
        dataMap.put("NonMarkings-RTV", Arrays.asList("Manhole"));
        dataMap.put("LineMarkingsPattern-RTV", Arrays.asList("Single solid line", "Single dashed line",
                "Double solid line", "Double dashed line", "Solid-dashed line", "Dashed-solid line",
                "Dash-dash-dash line", "Dash-solid-dash line"));
        dataMap.put("LineMarkingsColor-RTV", Arrays.asList("White", "Yellow"));
        dataMap.put("Weather-RTV", Arrays.asList("Sunny", "Cloudy", "Rainy", "Snow"));
        dataMap.put("LightDirection-RTV",Arrays.asList("Front","Rear","Side","Diffusion"));
        dataMap.put("IntersectType-RTV",Arrays.asList("Merges / divergy on freeway","Intersection on local road"));
        dataMap.put("RoadType-RTV",Arrays.asList("Freeway","Local road"));
        dataMap.put("LaneCount-RTV",Arrays.asList("No lane","Single lane","Two-three lane","Four-more lane"));
        dataMap.put("Country-RTV",Arrays.asList("USA","JPN","DEU","GBR"));
        dataMap.put("OtherFeatures-RTV", Arrays.asList("Toll booth","Grade Separation","Tunnel", "Gantry", "Cone", "Jersey wall", "Guard rail", "Curb"));

        StringBuilder builder = new StringBuilder();
        dataMap.forEach((key, values) -> values.forEach(attrValue -> {
            builder.append("INSERT INTO RecordAttributeValueInfo(attrCode, value, isValid, createTime, createBy, updateTime, updateBy) \n" +
                    "  VALUES(");
            builder.append("'"+key.trim().replaceAll(" ", "_").replaceAll("-", "_").toLowerCase()+"',");
            builder.append("'"+attrValue+"',");
            builder.append("1,now(),'admin',now(),'admin'");
            builder.append(");\n");
        }));
        System.out.println(builder.toString());
    }

    @Test
    public void sensorDataCode(){
        StringBuilder builder = new StringBuilder();
        for(int index=900;index<1026;index=index+2){
            builder.append("'SDSS-"+index+"',");
        }
        System.out.println(builder.toString());
    }

    @Test
    public void curlBatch(){
        String curlTemplate = "curl http://localhost:12306/re-send/anonymous/sensorDataCode/message\n";
        StringBuilder builder = new StringBuilder();
        for(int index=900;index<1026;index=index+2){
            if(index==910){
                continue;
            }
            builder.append(curlTemplate.replaceAll("sensorDataCode","SDSS-"+index));
        }
        System.out.println(builder.toString());
    }
}
