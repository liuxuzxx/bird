package org.lx.bird.javacore.string;

import org.junit.Test;

/**
 * 替换字符串的实验方法操作
 * Created by liuxu on 2018-01-16.
 */
public class ReplaceString {

    /**
     * 替换这种特殊符号，还是需要转义斜杠字符
     */
    @Test
    public void replaceDot(){
        String fileName = "user.dbo.cpp.java";
        System.out.println(fileName.replaceAll(".","_"));
    }
}
