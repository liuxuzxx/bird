package org.lx.bird.javacore.string;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.lx.bird.javacore.utils.StringFormatUtils;

import com.alibaba.fastjson.JSONObject;

/**
 * 分割字符串，自己不想写了，重复麻烦
 * Created by liuxu on 2017-09-21.
 */
public class SplitString {
    @Test
    public void splitString() {
        String property = "distribution.table.count.packageinfo=4\n" +
            "distribution.table.count.missionpackage=4\n" +
            "distribution.table.count.usermission=4\n" +
            "distribution.table.count.packageattributevalue=4\n" +
            "distribution.table.count.sensordata=4\n" +
            "distribution.table.count.packagesensordata=4\n" +
            "distribution.table.count.sensordatafile=4\n" +
            "distribution.table.count.sensordataconversion=4\n" +
            "distribution.table.count.sdsrecord=4\n" +
            "distribution.table.count.packagerecord=4\n" +
            "distribution.table.count.sdsrecordattributevalue=4\n" +
            "distribution.table.count.sdsrecordchannel=4\n" +
            "distribution.table.count.referenceroadmodel=4\n" +
            "distribution.table.count.packagerrm=4\n";
        String[] split = property.split("\n");
        StringBuilder builder = new StringBuilder();
        for (int index = 0; index < split.length; ++index) {
            String[] pros = split[index].split("=");
            builder.append("\n");
            builder.append("${" + pros[0] + "}");
        }
        System.out.println(builder.toString());
    }

    /**
     * 测试分割的结果个数
     */
    @Test
    public void splitCount() {
        String header = "Content-type: 600 :300:900:12306";
        String[] split = header.split(":", 12);
        System.out.println(JSONObject.toJSONString(split));
    }

    @Test
    public void splitHttpCodes() {
        String enumFormat = "{0}({1},\"{2}\")";

        String httpCodeInfo = "|100  | Continue                      | Section 6.2.1            |\n" +
            "   | 101  | Switching Protocols           | Section 6.2.2            |\n" +
            "   | 200  | OK                            | Section 6.3.1            |\n" +
            "   | 201  | Created                       | Section 6.3.2            |\n" +
            "   | 202  | Accepted                      | Section 6.3.3            |\n" +
            "   | 203  | Non-Authoritative Information | Section 6.3.4            |\n" +
            "   | 204  | No Content                    | Section 6.3.5            |\n" +
            "   | 205  | Reset Content                 | Section 6.3.6            |\n" +
            "   | 206  | Partial Content               | Section 4.1 of [RFC7233] |\n" +
            "   | 300  | Multiple Choices              | Section 6.4.1            |\n" +
            "   | 301  | Moved Permanently             | Section 6.4.2            |\n" +
            "   | 302  | Found                         | Section 6.4.3            |\n" +
            "   | 303  | See Other                     | Section 6.4.4            |\n" +
            "   | 304  | Not Modified                  | Section 4.1 of [RFC7232] |\n" +
            "   | 305  | Use Proxy                     | Section 6.4.5            |\n" +
            "   | 307  | Temporary Redirect            | Section 6.4.7            |\n" +
            "   | 400  | Bad Request                   | Section 6.5.1            |\n" +
            "   | 401  | Unauthorized                  | Section 3.1 of [RFC7235] |\n" +
            "   | 402  | Payment Required              | Section 6.5.2            |\n" +
            "   | 403  | Forbidden                     | Section 6.5.3            |\n" +
            "   | 404  | Not Found                     | Section 6.5.4            |\n" +
            "   | 405  | Method Not Allowed            | Section 6.5.5            |\n" +
            "   | 406  | Not Acceptable                | Section 6.5.6            |\n" +
            "   | 407  | Proxy Authentication Required | Section 3.2 of [RFC7235] |\n" +
            "   | 408  | Request Timeout               | Section 6.5.7            |\n" +
            "   | 409  | Conflict                      | Section 6.5.8            |\n" +
            "   | 410  | Gone                          | Section 6.5.9            |\n" +
            "   | 411  | Length Required               | Section 6.5.10           |\n" +
            "   | 412  | Precondition Failed           | Section 4.2 of [RFC7232] |\n" +
            "   | 413  | Payload Too Large             | Section 6.5.11           |\n" +
            "   | 414  | URI Too Long                  | Section 6.5.12           |\n" +
            "   | 415  | Unsupported Media Type        | Section 6.5.13           |\n" +
            "   | 416  | Range Not Satisfiable         | Section 4.4 of [RFC7233] |\n" +
            "   | 417  | Expectation Failed            | Section 6.5.14           |\n" +
            "   | 426  | Upgrade Required              | Section 6.5.15           |\n" +
            "   | 500  | Internal Server Error         | Section 6.6.1            |\n" +
            "   | 501  | Not Implemented               | Section 6.6.2            |\n" +
            "   | 502  | Bad Gateway                   | Section 6.6.3            |\n" +
            "   | 503  | Service Unavailable           | Section 6.6.4            |\n" +
            "   | 504  | Gateway Timeout               | Section 6.6.5            |\n" +
            "   | 505  | HTTP Version Not Supported    | Section 6.6.6            |";
        List<String> httpCodes = Arrays.asList(httpCodeInfo.split("\n"));

        List<String> enumHttpCodes = new ArrayList<>();
        httpCodes.forEach(httpCode -> {
            String[] elements = httpCode.split("\\|");
            enumHttpCodes.add(MessageFormat.format(enumFormat,
                StringFormatUtils.rectangularPulse(elements[2].trim().split(" ")),
                elements[1].trim(), elements[2].trim()));
        });

        enumHttpCodes.forEach(s -> System.out.println(s + ","));
    }

    @Test
    public void convertInteger() {
        String numberStr = "BDSR-000139867";
        String number = numberStr.substring("BDSR-".length());
        Integer count = Integer.valueOf(number);
        System.out.println(count);
    }

    @Test
    public void compareStr() {
        String left = "GPS:1506738772803,-32533789,614136593,0,0,0";
        String right = "GPS:1506738772867,-32533669,614136620,0,0,0";
        System.out.println(left.compareTo(right));
    }
}
