package org.lx.bird.javacore.string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;

/**
 * 测试String的内部内存使用
 * Created by liuxu on 2017-12-15.
 */
public class StringProperty {

    @Test
    public void interAndCons(){
        String userName = "userName";
        String adminName = "userName";
        String consName = new String("userName").intern();
        System.out.println(userName==adminName);
        System.out.println(consName==adminName);
    }

    @Test
    public void fileNameSuffix(){
        String[] fileNames = new String[]{"nike.mp4","address.mp4","anta.ch"};

        List<String> names = Arrays.asList(fileNames);
        Optional<String> first = names.stream()
            .filter(s -> s.endsWith(".sss"))
            .findFirst();
        System.out.println(first.get());
    }

    @Test
    public void stringZero(){
        String[] args = new String[0];
        System.out.println(args.length!=0);
    }

    @Test
    public void toArray(){
        List<String> strings = new ArrayList<>();
        String[] arr = strings.toArray(new String[strings.size()]);
        System.out.println(arr.length!=0);
    }
}
