package org.lx.bird.javacore.string;

import java.util.ArrayList;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by liuxu on 2017-08-23.
 */
public class AddString {

    @Test
    public void addString() {
        ArrayList<String> codes = new ArrayList<>();

        String code = "temp";
        for (int index = 0; index < 10; index++) {
            codes.add(code);
            code = "index" + index;
        }
        System.out.println(JSONObject.toJSONString(codes));
    }
}
