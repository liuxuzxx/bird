package org.lx.bird.javacore.jdkutils;

import java.util.Arrays;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

/**
 * 测试Arrays的类的工具方法
 * Created by liuxu on 2017-09-30.
 */
public class ArrayUtils {

    /**
     * 测试copy的动作
     */
    @Test
    public void copy(){
        String[] names = new String[]{"lx","zxx"};
        String[] copies = Arrays.copyOf(names, 4);
        System.out.println(JSONObject.toJSONString(copies));
    }
}
