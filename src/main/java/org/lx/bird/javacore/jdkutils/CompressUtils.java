package org.lx.bird.javacore.jdkutils;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.lf5.util.StreamUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 使用java的本身的JDK压缩工具类进行无损压缩
 * Create by xu.liu on 2018-03-26
 */
public class CompressUtils {

    private static Logger logger = LoggerFactory.getLogger(CompressUtils.class);

    @Test
    public void storeCompress(){
        String targetFileName = "/home/user/Pictures/test.zip";
        try(ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(targetFileName)))) {
            Path path = Paths.get("/home/user/Downloads");
            List<Path> collect = Files.list(path).collect(Collectors.toList());
            zipOutputStream.setMethod(ZipOutputStream.STORED);
            for (int index = 0; index < collect.size(); ++index) {
                Path tempPath = collect.get(index);
                try (InputStream inputStream = Files.newInputStream(tempPath)) {
                    zipOutputStream.putNextEntry(generateZipEntry(tempPath));
                    StreamUtils.copy(inputStream, zipOutputStream);
                }
            }
        } catch (IOException ex) {
            logger.error("压缩文件出现错误...",ex);
        }
    }

    private ZipEntry generateZipEntry(Path path) throws IOException {
        ZipEntry zipEntry = new ZipEntry(path.getFileName().toString());
        zipEntry.setMethod(ZipEntry.STORED);
        zipEntry.setSize(Files.size(path));
        zipEntry.setCrc(getCRC32(path));
        return zipEntry;
    }

    public static long getCRC32(Path path) throws IOException {
        byte[] buffer = new byte[8192];
        CRC32 crc32 = new CRC32();

        try(InputStream fileInputStream = Files.newInputStream(path)){
            int length;
            while((length=fileInputStream.read(buffer))!=-1){
                crc32.update(buffer,0,length);
            }
            return crc32.getValue();
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new IOException();
        }
    }
}
