package org.lx.bird.javacore.language;

import org.junit.Test;
import org.lx.bird.javacore.dto.UserDto;

/**
 * java对方法的重载实验
 * Created by liuxu on 2017-10-18.
 */
public class JavaOverrideMethod {
    public void say(Object obj){
        System.out.println("调用的是object参数的方法");
    }
    public void say(UserDto userDto){
        System.out.println("调用的是user的这个参数方法");
    }

    @Test
    public void test(){
        JavaOverrideMethod javaOverrideMethod = new JavaOverrideMethod();
        Object userDto = new UserDto();
        javaOverrideMethod.say(userDto);
    }
}
