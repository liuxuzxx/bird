package org.lx.bird.javacore.language;

import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

/**
 * 测试一个未经初始化的数组能否被赋值
 * Created by liuxu on 17-10-11.
 */
public class JavaArray {

    @Test
    public void assignArray(){
        String[] configs;
        String[] files = new String[]{"file","info"};
        configs = files;
        System.out.println(JSONObject.toJSONString(configs));
    }

    @Test
    public void now(){
        long time = System.currentTimeMillis();
        System.out.println(time);
    }

    @Test
    public void doubleCompare(){
        Double first = 0.0;

        System.out.println(first==0.0);
        System.out.println((Long.MAX_VALUE+"").length());
        System.out.println((Integer.MAX_VALUE+"").length());
        System.out.println(20006/1000);
    }
}
