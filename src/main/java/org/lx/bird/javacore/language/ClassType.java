package org.lx.bird.javacore.language;

import org.junit.Test;

/**
 * 测试一个类的getClass类型
 * Create by xu.liu on 2018-03-13
 */
public class ClassType {

    @Test
    public void fetchClassType(){
        Father father = new Son();
        System.out.println(father.getClass());
    }
}

class Father{

}
class Son extends Father{

}
