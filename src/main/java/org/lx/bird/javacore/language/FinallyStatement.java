package org.lx.bird.javacore.language;

import org.junit.Test;

/**
 * 查看一下放在finally之后的语句是否可以执行
 * Created by liuxu on 17-12-17.
 */
public class FinallyStatement {

    @Test
    public void finallyState(){
        try {
            System.out.println("try语句之中执行的操作...");
        }finally {
            System.out.println("finally语句之中执行的操作");
        }
        System.out.println("finally之后的语句");
    }
}
