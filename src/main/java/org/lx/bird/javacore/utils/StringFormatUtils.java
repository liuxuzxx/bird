package org.lx.bird.javacore.utils;

/**
 * 专注于字符串的格式化的工具类
 * Created by liuxu on 2017-10-12.
 */
public class StringFormatUtils {
    /**
     * 因为脉冲是矩形波，很相似这个单词大小使用下划线进行连接的形式
     *
     * @return 脉冲形式的单词
     */
    public static String pulse(String boundSymbol,String... words){
        StringBuilder builder = new StringBuilder();
        for(String word:words){
            builder.append(word.trim().toUpperCase()).append(boundSymbol);
        }
        return builder.deleteCharAt(builder.length()-1).toString();
    }

    public static String rectangularPulse(String... words){
        return pulse("_",words);
    }
}
