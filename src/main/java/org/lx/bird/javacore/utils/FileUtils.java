package org.lx.bird.javacore.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.stream.Stream;

/**
 * 文件的工具类
 * Created by liuxu on 2017-12-04.
 */
public final class FileUtils {

    public static void combineFiles(Path dirPath, Path combinePath) throws IOException {
        Stream<Path> stream = Files.list(dirPath);
        stream.forEach(path -> {
            try {
                byte[] bytes = Files.readAllBytes(path);
                Files.write(combinePath, bytes, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static void writeAll(String filePath, Object content) throws IOException {
        Path path = Paths.get(filePath);
        Files.write(path, content.toString().getBytes());
    }

    public static String readAll(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        return String.join("", Files.readAllLines(path));
    }

    private FileUtils() {

    }
}
