package org.lx.bird.javacore.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

/**
 * 测试java的正则表达式的使用
 * Create by xu.liu on 2018-04-09
 */
public class MatchPattern {
    private static final Pattern USER_NAME_PATTERN = Pattern.compile("^[A-Za-z0-9_.\\-@]{1,100}$");

    @Test
    public void userNamePattern(){
        Matcher matcher = USER_NAME_PATTERN.matcher("___.admin.-@.___");
        boolean matchFlag = matcher.matches();
        System.out.println(matchFlag);
    }
}
