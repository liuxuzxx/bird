package org.lx.bird.javacore.setcollection;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;

/**
 * 测试Java的一些集合的东西
 * Created by liuxu on 2017-09-01.
 */
public class MapTest {

    @Test
    public void mapIsCollection(){
        Map<String, String> map = new HashMap<>();
        System.out.println(map instanceof Collection);
    }

    @Test
    public void concurrentMap(){
        String key = "name";
        String value = "liuxu";
        Map<String,String> map = new ConcurrentHashMap<>();
        String put = map.put(key, value);
        System.out.println(put);
    }

    @Test
    public void concurrent(){
        String value = "value";
        String key = "key";
        Map<String, String> map = new ConcurrentHashMap<>();
        String put = map.put(key, value);
        System.out.println(map.get(key));
    }

    @Test
    public void absentValue(){
        Map<String, String> data = new ConcurrentHashMap<>();
        String value = data.putIfAbsent("key", "value");
        String newVal = data.putIfAbsent("key", "new_value");
        System.out.println(value);
        System.out.println(newVal);
    }
}
