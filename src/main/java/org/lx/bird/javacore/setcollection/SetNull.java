package org.lx.bird.javacore.setcollection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

/**
 * 测试set进行一个null的加入
 * Created by liuxu on 2017-11-06.
 */
public class SetNull {

    @Test
    public void addNull(){
        Set<String> set = new HashSet<>();
        boolean add = set.add(null);
        Object[] x = set.toArray();
        System.out.println(x);
    }

    @Test
    public void repeatRemove() throws IOException {
        List<String> contents = Files.readAllLines(Paths.get("/home/user/Pictures/links"));
        Set<String> codes = new HashSet<>();

        contents.forEach(line->{
            String[] split = line.split("/");
            codes.add(split[6]);
        });

        System.out.println(codes.size());
        System.out.println(contents.size());
    }
}
