package org.lx.bird.javacore.nio;

import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The Paths util test
 * Created by liuxu on 7/7/17.
 */
public class PathTest {

    @Test
    public void getFilePath(){
        Path path = Paths.get("/home/user/channel-pool.json");
        System.out.println(path.getFileName());
        System.out.println(path.getName(0));
        System.out.println(path.getParent().toString());
        File file = path.toFile();
        System.out.println(file.getAbsolutePath());
    }
}
