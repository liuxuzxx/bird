package org.lx.bird.javacore.nio;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.Selector;

/**
 * 测试文件Channel的和Selector的结合使用方式，比对ServerSocketChannel的使用的区别
 * Created by liuxu on 18-3-31.
 */
public class FileSelector {

    @Test
    public void initSelector() throws IOException {
        Selector selector = Selector.open();
        FileChannel channel = new FileInputStream("/home/liuxu/.vimrc").getChannel();
    }
}
