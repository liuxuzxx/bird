package org.lx.bird.javacore.nio;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


/**
 * 测试ByteBuffer的使用，limit，position，capacity
 * Created by liuxu on 2017-09-22.
 */
public class Buffer {
    @Test
    public void readOrWrite(){
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        buffer.put("你好，我是buffer".getBytes());
        buffer.flip();
        List<Byte> bytes = new ArrayList<Byte>();
        while(buffer.position()<buffer.limit()){
            bytes.add(buffer.get());
        }

        byte[] array = new byte[bytes.size()];
        for(int index=0;index<array.length;++index){
            array[index] = bytes.get(index);
        }
        buffer.array();
        System.out.println(new String(array));
    }
}
