package org.lx.bird.javacore.network;

import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * 尝试使用一下ServerSocket
 * Create by xu.liu on 2018-03-30
 */
public class ServerNioSocket {

    /**
     * 阻塞形式的Socket，使用于服务端
     */
    @Test
    public void init() throws IOException {
        ServerSocket server = new ServerSocket(6000);
        Socket client = server.accept();
        System.out.println(client);
    }

    @Test
    public void initChannel() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(2048);
        ServerSocketChannel server = ServerSocketChannel.open();
        ServerSocketChannel bindServer = server.bind(new InetSocketAddress(9000));
        SocketChannel accept = bindServer.accept();
        accept.read(buffer);
        System.out.println(new String(buffer.array()));
    }

    /**
     * 测试服务器端的ServerSocketChannel的使用和Selector的结合，好象是大家自己都有自己定义
     * 的一个Channel的实现，并不是多么的统一的接口
     *
     * @throws IOException
     */
    @Test
    public void initSelector() throws IOException {
        Selector selector = Selector.open();

        ServerSocketChannel server = ServerSocketChannel.open().bind(new InetSocketAddress(9000));
        server.configureBlocking(false);

        SelectionKey key = server.register(selector, SelectionKey.OP_ACCEPT);
        System.out.println(key);
    }
}
