package org.lx.bird.javacore.network;

import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * 远程操作Tomcat的TCP链接
 * Created by liuxu on 18-4-14.
 */
public class TomcatRemote {

    @Test
    public void remoteCloseTomcat() throws IOException {
        Socket client = new Socket("localhost", 9526);
        OutputStream outputStream = client.getOutputStream();
        outputStream.write("SHUTDOWN".getBytes());
        outputStream.close();
        client.close();
    }
}
