package org.lx.bird.javacore.sort;

import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.lx.bird.javacore.dto.CollectionUtil;
import org.lx.bird.javacore.dto.UserDto;

import java.util.List;

/**
 * The sort of List
 * Created by liuxu on 7/28/17.
 */
public class ListSort {

    @Test
    public void testSort() {
        List<UserDto> userDtos = CollectionUtil.batchUser(10);
        System.out.println(JSONObject.toJSONString(userDtos));
        userDtos.sort((left, right) -> {
            if (left.getUserId() > right.getUserId()) {
                return -1;
            } else {
                return 1;
            }
        });
        System.out.println(JSONObject.toJSONString(userDtos));
    }
}
