package org.lx.bird.javacore.sys;

import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

/**
 * java获取系统的properties信息
 * Created by liuxu on 2017-10-11.
 */
public class JavaSystemProperties {

    @Test
    public void getAllProperties(){
        Properties properties = System.getProperties();
        Set<Map.Entry<Object, Object>> entries = properties.entrySet();
        entries.forEach(objectObjectEntry -> {
            System.out.println(JSONObject.toJSONString(objectObjectEntry));
        });
    }
}
