package org.lx.bird.javacore.labmda;

import org.junit.Test;

import java.io.File;
import java.io.FileFilter;

/**
 * The basic lambda expression
 * Created by liuxu on 7/7/17.
 */
public class BasicLambda {

    @Test
    public void lambdaValue() {
        FileFilter filter = (File f) -> f.getName().endsWith(".java");
        AgeLambda age = () -> 20;
        System.out.println(age.nextAge());
    }
}
