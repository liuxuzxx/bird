package org.lx.bird.javacore.labmda;

/**
 * The age lambda function interface
 * Created by liuxu on 7/7/17.
 */
@FunctionalInterface
public interface AgeLambda {
    int nextAge();
}
