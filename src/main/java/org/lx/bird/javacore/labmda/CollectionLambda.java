package org.lx.bird.javacore.labmda;

import com.alibaba.fastjson.JSONObject;

import org.junit.Test;
import org.lx.bird.javacore.dto.CollectionUtil;
import org.lx.bird.javacore.dto.UserDto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A lambda expression for collection operation
 * Created by liuxu on 7/7/17.
 */
public class CollectionLambda {

    @Test
    public void forEachTest() {
        List<UserDto> userDtos = CollectionUtil.batchUser(20);
        userDtos.forEach(userDto -> userDto.setUserName("update the name"));
        System.out.println(JSONObject.toJSONString(userDtos));
    }

    @Test
    public void filterTest() {
        List<UserDto> userDtos = CollectionUtil.batchUser(20);
        userDtos.stream()
            .filter(userDto -> userDto.getUserId() % 4 == 0)
            .forEach(userDto -> System.out.println(JSONObject.toJSONString(userDto)));
    }

    @Test
    public void mapTest() {
        List<UserDto> userDtos = CollectionUtil.batchUser(30);
        List<Date> birthdays = userDtos.stream()
            .filter(userDto -> userDto.getUserId() % 400 == 0)
            .map(UserDto::getBirthday)
            .collect(Collectors.toList());
        System.out.println(birthdays.size());
    }

    @Test
    public void mapFirst() {
        List<UserDto> userDtos = CollectionUtil.batchUser(20);
        int userId = userDtos.stream()
            .findFirst()
            .get().getUserId();
        System.out.println(userId);
    }

    @Test
    public void sumTest() {
        List<Integer> integers = CollectionUtil.batctInt(30);
        int sum = integers.stream()
            .filter(integer -> integer > 20)
            .mapToInt(value -> value)
            .sum();
        System.out.println(sum);
    }

    @Test
    public void findFirstTest() {
        List<UserDto> userDtos = CollectionUtil.batchUser(20);
        UserDto first = userDtos.stream()
            .filter(userDto -> userDto.getUserId() > 300)
            .findFirst().orElse(null);
        System.out.println(JSONObject.toJSONString(first));
    }

    @Test
    public void emptyTest() {
        List<UserDto> userDtos = CollectionUtil.batchUser(20);
        List<UserDto> collect = userDtos.stream()
            .filter(userDto -> userDto.getUserId() > 200)
            .collect(Collectors.toList());
        System.out.println(collect.size());
    }

    @Test
    public void toMapTest() {
        List<UserDto> userDtos = CollectionUtil.batchUser(30);
        Map<Integer, String> collect = userDtos.stream()
            .collect(Collectors.toMap(userDto -> userDto.getUserId(), userDto -> userDto.getUserName()));
        System.out.println(collect.size());
    }

    @Test
    public void groupByTest() {
        List<UserDto> userDtos = CollectionUtil.batchUser(20);
        Map<String, List<UserDto>> collect = userDtos.stream()
            .collect(Collectors.groupingBy(o -> o.getUserName()));
        System.out.println(collect.size());
    }

    /**
     * 测试null的对象
     */
    @Test
    public void nullStream() {
        List<String> nulls = null;
        nulls.stream()
            .map(s -> true).collect(Collectors.toList());
    }

    @Test
    public void functionFunction(){
        List<UserDto> users = CollectionUtil.batchUser(30);
        users.stream()
                .filter(userDto -> userDto.getUserId()%3==0)
                .map(userDto -> userDto.getUserName()+":"+userDto.getBirthday())
                .collect(Collectors.toList());
    }

    @Test
    public void foreachUpdate(){
        /*List<UserDto> userDtos = CollectionUtil.batchUser(20);
        userDtos.forEach(userDto -> {
            userDto.setUserName("update");
        });
        System.out.println(JSONObject.toJSONString(userDtos));*/

        Integer[] arr = {10,20};
        System.out.println(JSONObject.toJSONString(arr));
        List<Integer> integers = Arrays.asList(arr);
        integers.forEach(integer -> {
            integer = integer + 90;
        });
        System.out.println(JSONObject.toJSONString(integers));

    }


}
