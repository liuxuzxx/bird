package org.lx.bird.javacore.reflect;

import org.junit.Test;
import org.lx.bird.javacore.dto.UserDto;

import java.lang.reflect.Field;

/**
 * 获取一个类的所有字段和对应的字段类型信息
 * Created by liuxu on 17-11-1.
 */
public class PropertyReflect {

    @Test
    public void reflectColumn(){
        Class clz = UserDto.class;
        Field[] declaredFields = clz.getDeclaredFields();
        for (Field field:declaredFields){
            String name = field.getName();
            Class<?> type = field.getType();
            System.out.println("name:"+name+"   type:"+type.getSimpleName());
        }
    }
}
