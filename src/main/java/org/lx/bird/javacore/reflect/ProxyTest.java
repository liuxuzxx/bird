package org.lx.bird.javacore.reflect;

import org.junit.Test;

import java.lang.reflect.Proxy;

/**
 * 代理实验的执行类
 * Created by liuxu on 17-9-2.
 */
public class ProxyTest {
    @Test
    public void proxy(){
        RoleService roleService = new RoleServiceImpl();
        RoleServiceProxy roleServiceProxy = new RoleServiceProxy(roleService);
        RoleService proxy = (RoleService)Proxy.newProxyInstance(RoleService.class.getClassLoader(), new Class[]{RoleService.class}, roleServiceProxy);
        proxy.checkRole(null);
    }
}
