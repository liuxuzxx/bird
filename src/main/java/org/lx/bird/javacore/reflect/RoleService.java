package org.lx.bird.javacore.reflect;

import org.lx.bird.javacore.dto.RoleDto;

/**
 * 用户方面的服务，使用java的InvocationHandler的代理模式实验工作
 * Created by liuxu on 17-9-2.
 */
public interface RoleService {
    void checkRole(RoleDto roleDto);
}
