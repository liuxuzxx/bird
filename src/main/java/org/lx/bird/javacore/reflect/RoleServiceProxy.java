package org.lx.bird.javacore.reflect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Role角色服务的对象的代理模式实验
 * Created by liuxu on 17-9-2.
 */
public class RoleServiceProxy implements InvocationHandler {
    private Object obj;

    public RoleServiceProxy(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("执行jdk的动态代理进行before操作");
        Object result = method.invoke(obj, args);
        System.out.println(proxy.getClass().getName());
        System.out.println("after操作");
        return null;
    }
}
