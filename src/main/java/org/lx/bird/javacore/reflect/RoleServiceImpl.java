package org.lx.bird.javacore.reflect;

import com.alibaba.fastjson.JSONObject;
import org.lx.bird.javacore.dto.RoleDto;

/**
 * 角色服务的实现，常识使用jdk的动态代理动作
 * Created by liuxu on 17-9-2.
 */
public class RoleServiceImpl implements RoleService {
    @Override
    public void checkRole(RoleDto roleDto) {
        System.out.println("Role角色信息内容对象：" + JSONObject.toJSONString(roleDto));
    }
}
