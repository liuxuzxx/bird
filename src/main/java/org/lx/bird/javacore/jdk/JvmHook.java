package org.lx.bird.javacore.jdk;

/**
 * 实验JVM的关闭狗子，竟然还有这种操作
 * Created by liuxu on 18-4-13.
 */
public class JvmHook {
    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(new CloseHook()));
        System.out.println("Main开始执行操作...");
        throw new NullPointerException("Throw null exception...");
    }
}

class CloseHook implements Runnable{

    @Override
    public void run() {
        System.out.println("回首资源...");
    }
}
