package org.lx.bird.javacore.jdk;

import org.junit.Test;

/**
 * 关于Runtime的一些信息内容
 * Create by xu.liu on 2018-03-30
 */
public class RuntimeInformation {

    /**
     * 获取的是实际上是电脑的处理器的线程数
     * 换句话说，我这个电脑是：4核8线程.和内存
     */
    @Test
    public void countProcessors(){
        int count = Runtime.getRuntime().availableProcessors();
        System.out.println(count);
        long maxMemory = Runtime.getRuntime().maxMemory();
        System.out.println(maxMemory/(1024*1024*1024));
    }
}
