package org.lx.bird.javacore.dto;

/**
 * 生成一个属性信息的对象，主要是用来生成一个json形式的字符串
 * Created by liuxu on 2017-08-29.
 */
public class AttributesInfo {
    private String code;
    private String value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
