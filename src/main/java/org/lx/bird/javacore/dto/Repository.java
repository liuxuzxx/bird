package org.lx.bird.javacore.dto;

/**
 * Created by liuxu on 2017-10-26.
 */
public class Repository {
    private String name;
    private Boolean isValid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean valid) {
        isValid = valid;
    }
}
