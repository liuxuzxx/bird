package org.lx.bird.javacore.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * A domain entity for upload file list info
 * Created by liuxu on 2017-08-30.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SdsFilesInfo {
    private List<String> rawDataFileList;
    private List<String> recordFileList;

    public List<String> getRawDataFileList() {
        return rawDataFileList;
    }

    public void setRawDataFileList(List<String> rawDataFileList) {
        this.rawDataFileList = rawDataFileList;
    }

    public List<String> getRecordFileList() {
        return recordFileList;
    }

    public void setRecordFileList(List<String> recordFileList) {
        this.recordFileList = recordFileList;
    }
}
