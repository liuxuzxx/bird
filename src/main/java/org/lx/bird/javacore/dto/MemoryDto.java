package org.lx.bird.javacore.dto;

import java.nio.ByteBuffer;

/**
 * 专门负责测试jvm的heap内存分配的dto对象
 * Created by liuxu on 17-10-17.
 */
public class MemoryDto {

    private ByteBuffer buffer;

    public MemoryDto() {
        buffer = ByteBuffer.allocate(1024 * 10);
    }
}
