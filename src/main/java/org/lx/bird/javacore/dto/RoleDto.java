package org.lx.bird.javacore.dto;

/**
 * 角色的entity的对象
 * Created by liuxu on 17-9-2.
 */
public class RoleDto {
    private String roleName;
    private String password;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
