package org.lx.bird.javacore.dto;

import java.util.Date;

/**
 * The user dto entity
 * Created by liuxu on 7/7/17.
 */
public class UserDto {
    private int userId;
    private String userName;
    private Date birthday;

    public UserDto() {
        System.out.println("构造UserDto对象了...");
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
