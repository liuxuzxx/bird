package org.lx.bird.javacore.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A util tool for user dto
 * Created by liuxu on 7/7/17.
 */
public class CollectionUtil {

    public static List<UserDto> batchUser(int number) {
        List<UserDto> userDtos = new ArrayList<>();
        for (int index = 0; index < number; ++index) {
            UserDto userDto = new UserDto();
            userDto.setUserId(index);
            userDto.setUserName("userName_" + index);
            userDto.setBirthday(new Date());
            userDtos.add(userDto);
        }
        return userDtos;
    }

    public static List<Integer> batctInt(int count) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int index = 0; index < count; ++index) {
            integers.add(index);
        }
        return integers;
    }
}
