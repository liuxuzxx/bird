package org.lx.bird.javacore.enumtype;

import org.junit.Test;

/**
 * Create by xu.liu on 2018-03-28
 */
public class SwitchEnum {

    @Test
    public void switcher() {
        String suffix ;
        ColumnType type = ColumnType.VARCHAR;
        switch (type) {
            case INT:
                System.out.println("INT类型");
                suffix = ".int";
                break;
            case DOUBLE:
            case VARCHAR:
                System.out.println("字符串类型");
                suffix = ".string";
                break;
            default:
                throw new UnsupportedOperationException("不支持这种类型的操作");
        }

        System.out.println(suffix);
    }
}
