package org.lx.bird.javacore.enumtype;

/**
 * 数据库字段的enum
 * Created by liuxu on 17-9-15.
 */
public enum ColumnType {
    INT("INT"),
    TINYINT("TINYINT"),
    VARCHAR("VARCHAR"),
    DATETIME("DATETIME"),
    TIMESTAMP("TIMESTAMP"),
    DOUBLE("DOUBLE");

    private final String name;
    ColumnType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
