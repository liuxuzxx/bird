package org.lx.bird.javacore.ygomi;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.lx.bird.javacore.utils.FileUtils;

/**
 * 处理confluence文档的oea的attribute的工具
 * Create by xu.liu on 2018-03-14
 */
public class ConfluenceOeaAttributeTool {

    @Test
    public void confluceOeaAttribute() throws IOException {
        String content = FileUtils.readAll("/home/user/project/bird/src/main/java/org/lx/bird/javacore/ygomi/oea_attribute.html");
        System.out.println(content);
        Document document = Jsoup.parse(content);
        Element body = document.body();

        parseTable(body);
    }

    private void parseTable(Element body) {
        Element table = body.getElementById("oeaTable");
        Elements trs = table.getElementsByTag("tr");
        for(int index=1;index<trs.size();++index){
            Element infoTr = trs.get(index);
            Elements ths = infoTr.getElementsByTag("th|tr");
            System.out.println();
        }
    }
}

class OeaTableDto{
    private String category;
    private String oeaName;
    private String valueSpace;
    private String priority;
    private String sdsVersion;
    private String remark;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getOeaName() {
        return oeaName;
    }

    public void setOeaName(String oeaName) {
        this.oeaName = oeaName;
    }

    public String getValueSpace() {
        return valueSpace;
    }

    public void setValueSpace(String valueSpace) {
        this.valueSpace = valueSpace;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getSdsVersion() {
        return sdsVersion;
    }

    public void setSdsVersion(String sdsVersion) {
        this.sdsVersion = sdsVersion;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
