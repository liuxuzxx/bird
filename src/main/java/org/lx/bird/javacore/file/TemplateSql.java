package org.lx.bird.javacore.file;

import java.io.IOException;
import java.text.MessageFormat;

import org.junit.Test;
import org.lx.bird.javacore.utils.FileUtils;

/**
 * 写入模板的sql代码
 * Created by liuxu on 2018-01-22.
 */
public class TemplateSql {

    @Test
    public void insertAuditionSql() throws IOException {
        String singleSql = "ALTER TABLE SdsRecord{0} ADD COLUMN convertStatus TINYINT NOT NULL DEFAULT 1 COMMENT '1:converting,2:convert success,-1:convert failed';\n" +
            "ALTER TABLE PackageInfo{0} add packageName VARCHAR(100) NOT NULL DEFAULT \"\";\n";
        StringBuilder dbSql = new StringBuilder();
        for (int x = 0; x < 16; x++) {
            dbSql = dbSql.append("use sds_" + x + ";\n");
            for (int y = 0; y < 16; y++) {
                dbSql.append(singleSql.replaceAll("\\{0\\}","_"+y));
            }
        }
        FileUtils.writeAll("/home/user/Downloads/2018_1_12.sql", dbSql.toString());
    }
}
