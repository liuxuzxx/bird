package org.lx.bird.javacore.file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.lx.bird.javacore.utils.FileUtils;

/**
 * java 7之后提供的Files新类进行一个使用和测试
 * Created by liuxu on 2017-11-01.
 */
public class FilesTest {

    /**
     * 测试检查文件类型
     */
    @Test
    public void checkType() throws IOException {
        Path path = Paths.get("~/jdk.tar.gz");
        String type = Files.probeContentType(path);
        System.out.println(type);
    }

    @Test
    public void combineSql() throws IOException {
        String combine = "/home/user/project/sql";
        FileUtils.combineFiles(Paths.get(combine),Paths.get(combine,"sds_insert.sql"));
    }

    @Test
    public void suffixFile() throws IOException {
        Path path = Paths.get("/home/user");
        Files.list(path).forEach(sonPath->{
            if(sonPath.toString().endsWith(".log")){
                System.out.println(sonPath.toString());
            }
        });
    }
}
