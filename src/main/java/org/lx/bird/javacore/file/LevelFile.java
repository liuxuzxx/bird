package org.lx.bird.javacore.file;

import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 实验具有层级file的时候是否可以新建文件对象
 * Created by liuxu on 17-10-24.
 */
public class LevelFile {

    @Test
    public void level() throws IOException {
        String filePath = "/home/liuxu/data/zxx.txt";
        File file = new File(filePath);
        boolean mkdirs = file.mkdirs();

        FileOutputStream fileOutputStream = new FileOutputStream(filePath);
        fileOutputStream.write("innnn".getBytes());
        fileOutputStream.close();
    }
}
