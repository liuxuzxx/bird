package org.lx.bird.fly.spider;

import com.alibaba.fastjson.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.lx.bird.javacore.utils.FileUtils;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

/**
 * gradle的批量下载
 * Created by liuxu on 17-11-24.
 * 批量的下载gradle的压缩包
 * Created by liuxu on 2017-11-24.
 */
public class GradleBat {

    @Test
    public void batObtain() throws IOException {
        List<String> androidHrefs = null;
        Document document = Jsoup.parse(new URL("https://developer.android.com/guide/index.html"), 5 * 1000 * 10);
        Element ul = document.getElementById("nav");
        androidHrefs = ul.children()
                .stream()
                .map(listElement -> {
                    Elements aElement = listElement.getElementsByTag("a");
                    return aElement.get(0).attr("href");
                }).collect(Collectors.toList());

        System.out.println(JSONObject.toJSONString(androidHrefs));
    }

    public void batDown() throws IOException {
        Document document = Jsoup.parse(new URL("http://services.gradle.org/distributions/"), 10 * 10000);
        Elements ulElemets = document.getElementsByClass("items");
        Element ul = ulElemets.get(0);
        Elements aEles = ul.getElementsByTag("a");
        List<String> hrefs = aEles.stream()
                .map(element -> "wget http://downloads.gradle.org" + element.attr("href")).collect(Collectors.toList());
        FileUtils.writeAll("/home/liuxu/Documents/gradle/gradle_sh.sh", String.join("\n", hrefs));
    }


}
