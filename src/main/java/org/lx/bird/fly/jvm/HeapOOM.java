package org.lx.bird.fly.jvm;

import org.lx.bird.javacore.dto.MemoryDto;
import org.lx.bird.javacore.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试heap的out of memory.测试jvm的heap内存溢出
 * Created by liuxu on 17-10-17.
 */
public class HeapOOM {
    public static List<MemoryDto> memoryDtos = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        while (true) {
            MemoryDto memoryDto = new MemoryDto();
            memoryDtos.add(memoryDto);
            if (memoryDtos.size() % 100 == 0) {
                Thread.sleep(200);
                System.out.println("user对象的个数是：" + memoryDtos.size());
            }
        }
    }
}
