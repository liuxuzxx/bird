package org.lx.bird.fly.share.persistence;

import org.lx.bird.fly.share.db.DBUtils;
import org.lx.bird.fly.share.dto.ShareInfoDto;
import org.lx.bird.fly.share.parser.EntityHandler;
import org.lx.bird.fly.share.parser.HtmlParser;
import org.lx.bird.fly.share.parser.impl.JsoupHtmlParser;
import org.lx.bird.fly.share.parser.impl.ShareInfoHandler;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * 决定使用多线程进行操作share info插入数据库当中去
 * Created by liuxu on 17-9-15.
 */
public class ShareInfoTask implements Runnable {
    private String shareCode;
    private List<File> shareInfoFiles;
    private static HtmlParser<ShareInfoDto> htmlParser;
    private static DBUtils dbUtils = new DBUtils();

    static {
        htmlParser = new JsoupHtmlParser<>(new ShareInfoHandler());
    }

    public ShareInfoTask(String shareCode, List<File> shareInfoFiles) {
        this.shareCode = shareCode;
        this.shareInfoFiles = shareInfoFiles;
    }

    @Override
    public void run() {
        shareInfoFiles.forEach(file -> {
            try {
                htmlParser.feed(file);
                List<ShareInfoDto> shareInfoDtos = htmlParser.parserTs("tr", "table_bg001 border_box limit_sale");
                shareInfoDtos.forEach(shareInfoDto -> shareInfoDto.setShareCode(shareCode));
                dbUtils.persistence(shareInfoDtos);
                System.out.println("----------------------------" + shareCode + ":" + shareInfoDtos.size());
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
        });
        System.out.println("我负责解析的是：" + shareCode + "   一共是：" + shareInfoFiles.size() + "  个文件");
    }
}
