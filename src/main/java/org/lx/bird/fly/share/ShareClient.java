package org.lx.bird.fly.share;

import org.junit.Test;
import org.lx.bird.fly.share.db.DBUtils;
import org.lx.bird.fly.share.db.dao.ShareInfoDao;
import org.lx.bird.fly.share.dto.ShareInfoDto;
import org.lx.bird.fly.share.parser.EntityHandler;
import org.lx.bird.fly.share.parser.impl.JsoupHtmlParser;
import org.lx.bird.fly.share.parser.impl.ShareInfoHandler;
import org.lx.bird.fly.share.persistence.ShareFileUtils;
import org.lx.bird.fly.share.persistence.ShareInfoTask;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * share info的客户端测试
 * Created by liuxu on 17-9-15.
 */
public class ShareClient {

    @Test
    public void parserTrInfo() throws IOException, ParseException {
        File file = new File("/home/liuxu/project/python/share/html/000001/000001_2007_1.html");
        EntityHandler shareInfoHandler = new ShareInfoHandler();
        JsoupHtmlParser<ShareInfoDto> shareInfoDtoJsoupHtmlParser = new JsoupHtmlParser<ShareInfoDto>(shareInfoHandler);
        shareInfoDtoJsoupHtmlParser.feed(file);
        List<ShareInfoDto> shareInfoDtos = shareInfoDtoJsoupHtmlParser.parserTs("tr", "table_bg001 border_box limit_sale");
        System.out.println("infos");
    }

    @Test
    public void obtainFile() {
        Map<String, List<File>> stringFileMap = ShareFileUtils.obtainShares("/home/liuxu/project/python/share/html/");
        System.out.print("");
    }

    /**
     * 执行的客户端，我们是一个文件夹给一个线程去执行
     */
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Map<String, List<File>> stringFileMap = ShareFileUtils.obtainShares("/home/liuxu/project/python/share/html/");
        stringFileMap.forEach((shareCode, shareInfoFiles) -> new Thread(new ShareInfoTask(shareCode, shareInfoFiles)).start());
        long end = System.currentTimeMillis();
        System.out.println("花费的时间为："+(end-start)+" ms");
    }

    @Test
    public void test(){
        ShareInfoDto shareInfoDto = new ShareInfoDto();
        shareInfoDto.setShareCode("001");
        shareInfoDto.setDateTime(new Date());
        shareInfoDto.setSwing(3.00);
        shareInfoDto.setTurnoverRate(0.36);
        shareInfoDto.setMoney(2000);
        shareInfoDto.setVolume(200);
        shareInfoDto.setRiseFallRate(0.23);
        shareInfoDto.setEndPrice(2.36);
        shareInfoDto.setStartPrice(5023);
        shareInfoDto.setMinPrice(0.36);
        shareInfoDto.setMaxPrice(6.36);
        shareInfoDto.setRiseFall(0.369);
        DBUtils dbUtils = new DBUtils();
        for(int index=0;index<10;++index) {
            dbUtils.signal(shareInfoDto);
            dbUtils.signal(shareInfoDto);
        }
    }
}
