package org.lx.bird.fly.share.parser;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * java的解析html页面的接口
 * Created by liuxu on 17-9-15.
 */
public interface HtmlParser<T> {
    /**
     * 总是逃不掉喂数据信息，要不然解析什么玩意啊
     * @param data 需要解析的html数据字符串
     */
    void feed(String data);

    /**
     * 从文件当中喂数据
     * @param file html文件
     */
    void feed(File file) throws IOException;

    /**
     * 根据tag提取tag之间的数据字符串
     * @param tag tag的名字
     * @param className
     * @return tag之间的数据字符串
     */
    String getTagData(String tag, String className);

    /**
     * 解析tag标签之间的数据信息，反射出来一个对象
     * @param tag 标签
     * @param className
     * @return
     */
    T parserT(String tag, String className);

    /**
     * 解析tag之间数据为T的集合对象
     * @param tag 标签
     * @param className
     * @return T对象的集合
     */
    List<T> parserTs(String tag, String className) throws ParseException;
}
