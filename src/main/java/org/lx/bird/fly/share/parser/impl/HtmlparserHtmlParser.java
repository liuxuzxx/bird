package org.lx.bird.fly.share.parser.impl;

import org.lx.bird.fly.share.parser.HtmlParser;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * htmlparser的提供的解析html的实现
 * Created by liuxu on 17-9-15.
 */
public class HtmlparserHtmlParser<T> implements HtmlParser<T> {
    @Override
    public void feed(String data) {

    }

    @Override
    public void feed(File file) throws IOException {

    }

    @Override
    public String getTagData(String tag, String className) {
        return null;
    }

    @Override
    public T parserT(String tag, String className) {
        return null;
    }

    @Override
    public List<T> parserTs(String tag, String className) {
        return null;
    }
}
