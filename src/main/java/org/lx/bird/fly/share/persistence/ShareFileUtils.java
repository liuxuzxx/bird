package org.lx.bird.fly.share.persistence;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 处理share info文件夹的工具类
 * Created by liuxu on 17-9-15.
 */
public class ShareFileUtils {

    public static Map<String,List<File>> obtainShares(String parentPath){
        File parent = new File(parentPath);
        File[] sons = parent.listFiles();
        Map<String, List<File>> codeFileMap = new HashMap<>();
        for(File son:sons){
            if(son.isDirectory()){
                codeFileMap.put(son.getName(), Arrays.asList(son.listFiles()));
            }
        }
        return codeFileMap;
    }
}
