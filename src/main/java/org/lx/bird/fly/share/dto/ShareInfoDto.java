package org.lx.bird.fly.share.dto;

import java.util.Date;

/**
 * share info的dto
 * Created by liuxu on 17-9-15.
 */
public class ShareInfoDto {
    private int infoId;
    private String shareCode;
    private Date dateTime;
    private double startPrice;
    private double endPrice;
    private double maxPrice;
    private double minPrice;
    private double riseFall;
    private double riseFallRate;
    private int volume;
    private double money;
    private double swing;
    private double turnoverRate;

    public int getInfoId() {
        return infoId;
    }

    public void setInfoId(int infoId) {
        this.infoId = infoId;
    }

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(double startPrice) {
        this.startPrice = startPrice;
    }

    public double getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(double endPrice) {
        this.endPrice = endPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getRiseFall() {
        return riseFall;
    }

    public void setRiseFall(double riseFall) {
        this.riseFall = riseFall;
    }

    public double getRiseFallRate() {
        return riseFallRate;
    }

    public void setRiseFallRate(double riseFallRate) {
        this.riseFallRate = riseFallRate;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getSwing() {
        return swing;
    }

    public void setSwing(double swing) {
        this.swing = swing;
    }

    public double getTurnoverRate() {
        return turnoverRate;
    }

    public void setTurnoverRate(double turnoverRate) {
        this.turnoverRate = turnoverRate;
    }
}
