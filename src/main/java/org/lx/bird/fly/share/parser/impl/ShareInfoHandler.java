package org.lx.bird.fly.share.parser.impl;

import org.lx.bird.fly.share.dto.ShareInfoDto;
import org.lx.bird.fly.share.parser.EntityHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * share info 的实体解析handler
 * Created by liuxu on 17-9-15.
 */
public class ShareInfoHandler implements EntityHandler<ShareInfoDto> {

    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public List<ShareInfoDto> fillEntity(List<String> properties) {
        List<ShareInfoDto> shareInfoDtos = new ArrayList<>();
        for (int index = 1; index < properties.size(); ++index) {
            String property = properties.get(index).replaceAll(",", "");
            String[] split = property.split(" ");

            try {
                ShareInfoDto shareInfoDto = new ShareInfoDto();
                shareInfoDto.setDateTime(format.parse(split[0]));
                shareInfoDto.setStartPrice(Double.parseDouble(split[1]));
                shareInfoDto.setMaxPrice(Double.parseDouble(split[2]));
                shareInfoDto.setMinPrice(Double.parseDouble(split[3]));
                shareInfoDto.setEndPrice(Double.parseDouble(split[4]));
                shareInfoDto.setRiseFall(Double.parseDouble(split[5]));
                shareInfoDto.setRiseFallRate(Double.parseDouble(split[6]));
                shareInfoDto.setVolume(Integer.parseInt(split[7]));
                shareInfoDto.setMoney(Double.parseDouble(split[8]));
                shareInfoDto.setSwing(Double.parseDouble(split[9]));
                shareInfoDto.setTurnoverRate(Double.parseDouble(split[10]));
                shareInfoDtos.add(shareInfoDto);
            } catch (Exception ex) {
                continue;
            }
        }
        return shareInfoDtos;
    }
}
