package org.lx.bird.fly.share.parser;

import java.text.ParseException;
import java.util.List;

/**
 * 关于解析出来之后，实体的生成控制器
 * Created by liuxu on 17-9-15.
 */
public interface EntityHandler<T> {
    /**
     * 根据属性的集合填充我们的dto实体对象
     * @param properties 属性集合
     * @return 填充好的对象
     */
    List<T> fillEntity(List<String> properties);
}
