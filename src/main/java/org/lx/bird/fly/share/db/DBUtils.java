package org.lx.bird.fly.share.db;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.lx.bird.fly.share.db.dao.ShareInfoDao;
import org.lx.bird.fly.share.dto.ShareInfoDto;

import java.io.IOException;
import java.util.List;

/**
 * 负责share info数据插入到数据库当中的工具类
 * Created by liuxu on 17-9-16.
 */
public class DBUtils {
    private static final String MYBATIS_CONFIG = "org/lx/bird/fly/share/mybatis-config.xml";
    private static SqlSessionFactory build;
    private static SqlSession sqlSession ;
    private ShareInfoDao shareInfoDao;

    public DBUtils(){
        shareInfoDao = sqlSession.getMapper(ShareInfoDao.class);
    }

    static{
        try {
            build = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader(MYBATIS_CONFIG));
            sqlSession = build.openSession();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void persistence(List<ShareInfoDto> shareInfoDtoList){
        System.out.println("DButils******************************************");
        if(shareInfoDtoList.size()>0) {
            try {
                shareInfoDao.batchInsert(shareInfoDtoList);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        sqlSession.commit();
    }

    public synchronized void signal(ShareInfoDto shareInfoDto){
        shareInfoDao.insertShareInfo(shareInfoDto);
        sqlSession.commit();
    }
}
