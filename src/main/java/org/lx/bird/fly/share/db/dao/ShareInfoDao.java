package org.lx.bird.fly.share.db.dao;

import org.lx.bird.fly.share.dto.ShareInfoDto;

import java.util.List;

/**
 * 负责share info的接口的函数
 * Created by liuxu on 17-9-16.
 */
public interface ShareInfoDao {

    /**
     * 批量插入数据信息
     * @param shareInfoDtos 插入的数据对象集合
     */
    void batchInsert(List<ShareInfoDto> shareInfoDtos);

    /**
     * 插入单个ShareInfo信息对象
     * @param shareInfoDto 等待插入的数据对象
     */
    void insertShareInfo(ShareInfoDto shareInfoDto);
}
