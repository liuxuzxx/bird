package org.lx.bird.fly.share.parser.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.lx.bird.fly.share.parser.EntityHandler;
import org.lx.bird.fly.share.parser.HtmlParser;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * jsoup实现的html解析
 * Created by liuxu on 17-9-15.
 */
public class JsoupHtmlParser<T> implements HtmlParser<T> {

    private Document document;

    private EntityHandler<T> entityHandler;

    public JsoupHtmlParser(EntityHandler entityHandler) {
        this.entityHandler = entityHandler;
    }

    @Override
    public void feed(String data) {
        document = Jsoup.parse(data);
    }

    @Override
    public synchronized void feed(File file) throws IOException {
        document = Jsoup.parse(file, "UTF-8");
    }

    @Override
    public String getTagData(String tag, String className) {
        return parserData(tag, className);

    }

    @Override
    public T parserT(String tag, String className) {
        return null;
    }

    @Override
    public synchronized List<T> parserTs(String tag, String className) throws ParseException {
        List<String> parser = parser(tag, className);
        return entityHandler.fillEntity(parser);
    }

    /**
     * 解析html的标签数据
     */
    private List<String> parser(String tag, String className) {
        List<String> elementInfos = new LinkedList<>();
        Elements elements = document.getElementsByClass(className);
        elements.forEach(element -> {
            Elements tagElements = element.getElementsByTag(tag);
            tagElements.forEach(tagElement -> {
                elementInfos.add(tagElement.text());
            });
        });
        return elementInfos;
    }

    private String parserData(String tag, String className) {
        List<String> elementInfos = new LinkedList<>();
        Elements elements = document.getElementsByClass(className);
        return elements.text();
    }
}
