package org.lx.bird.fly.idea.impl;

import org.lx.bird.fly.idea.ReformatCode;

import java.nio.file.Path;

/**
 * do the idea reformat code.
 * Created by liuxu on 2017-08-09.
 */
public class CPPReformatCode implements ReformatCode {
    @Override
    public String formatName() {
        return "CPP";
    }

    public void reformat(Path codePath) {

    }
}
