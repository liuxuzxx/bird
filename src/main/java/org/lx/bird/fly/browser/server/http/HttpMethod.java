package org.lx.bird.fly.browser.server.http;

/**
 * Http的方法枚举
 */
public enum HttpMethod {
    UN_KNOW_METHOD("UN_KNOW_METHOD",0),
    GET("GET", 1),
    POST("POST", 2),
    PUT("PUT", 3),
    DELETE("DELETE", 4),
    OPTIONS("OPTIONS", 5),
    HEAD("HEAD", 6),
    TRACE("TRACE", 7);
    private String methodName;
    private int methodCode;

    HttpMethod(String methodName, int methodCode) {
        this.methodName = methodName;
        this.methodCode = methodCode;
    }

    public String getMethodName() {
        return this.methodName;
    }

    public int getMethodCode() {
        return this.methodCode;
    }

    public static HttpMethod matchMethod(String methodName){
        HttpMethod[] values = HttpMethod.values();
        for (HttpMethod value : values) {
            if(value.getMethodName().equalsIgnoreCase(methodName)){
                return value;
            }
        }
        return UN_KNOW_METHOD;
    }
}
