package org.lx.bird.fly.browser.server.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * http协议的io方面的工具类
 * Created by liuxu on 2017-09-29.
 */
public class HttpIOUtils {
    private static Logger logger = LoggerFactory.getLogger(HttpIOUtils.class);

    public static byte[] readFile(Path path) throws IOException {
        return Files.readAllBytes(path);
    }

    public static byte[] readFile(String filePath) throws IOException {
        return readFile(Paths.get(filePath));
    }

    public static void writeFile(String filePath, String content) throws IOException {
        writeFile(Paths.get(filePath), content);
    }

    public static void writeFile(Path path, String content) throws IOException {
        Files.write(path, content.getBytes());
    }

    public static void writeAppend(File file, String content) {
        try (Writer out = new FileWriter(file, true)) {
            out.write(content);
        } catch (IOException e) {
            logger.debug("追加信息到文件：" + file.getAbsolutePath() + " 出现IO错误.");
            e.printStackTrace();
        }
    }

    public static void writeAppend(String filePath,String content){
        writeAppend(new File(filePath),content);
    }
}
