package org.lx.bird.fly.browser.server.http.body.impl;

import org.lx.bird.fly.browser.server.http.body.HttpBodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * http的body是文件部分的处理器
 * Created by liuxu on 17-10-9.
 */
public class FileBodyHandler implements HttpBodyHandler {
    private String filePath;
    private static Logger logger = LoggerFactory.getLogger(FileBodyHandler.class);

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void handler(byte[] bodies) {
        save(bodies);
    }

    /**
     * 保存body的数据到指定的文件路径下面
     * @param bodies body的字节数组
     */
    private void save(byte[] bodies){
        try {
            if (filePath==null){
                logger.debug("filePath为null，所以这是当前路径为保存路径");
                filePath = "./temp.lx";
            }
            Files.write(Paths.get(filePath),bodies);
        } catch (IOException e) {
            logger.debug("存储body的文件信息出现IO错误");
            e.printStackTrace();
        }
    }
}
