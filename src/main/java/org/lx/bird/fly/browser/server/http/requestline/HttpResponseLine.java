package org.lx.bird.fly.browser.server.http.requestline;

import org.lx.bird.fly.browser.server.http.HttpConstant;
import org.lx.bird.fly.browser.server.http.HttpStatus;
import org.lx.bird.fly.browser.server.http.HttpVersion;

/**
 * http协议的回复行 版本 状态码 状态字符串
 * Created by liuxu on 2017-10-20.
 */
public class HttpResponseLine {
    private HttpVersion version;
    private HttpStatus status;

    public HttpResponseLine(HttpVersion version, HttpStatus status) {
        this.version = version;
        this.status = status;
    }

    public String reponseLine(){
        StringBuilder builder = new StringBuilder();
        builder.append(version.getVersionName());
        builder.append(HttpConstant.HTTP_MESSAGE_REQUEST_SPLIT);
        builder.append(status.getValue());
        builder.append(HttpConstant.HTTP_MESSAGE_REQUEST_SPLIT);
        builder.append(status.getReasonPhrase());
        builder.append(HttpConstant.HTTP_MESSAGE_LINE_SPLIT);
        return builder.toString();
    }
}
