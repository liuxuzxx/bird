package org.lx.bird.fly.browser.server.utils;

import java.util.Arrays;

/**
 * 因为java操作数组有点危险，但是有时候不得不使用数组进行一个处理
 * Created by liuxu on 2017-09-30.
 */
public class ArrayUtils {
    public static <T> T[] alignArray(T[] src,int alignNumber){
        return Arrays.copyOf(src,alignNumber);
    }
}
