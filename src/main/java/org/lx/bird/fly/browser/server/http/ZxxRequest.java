package org.lx.bird.fly.browser.server.http;

import org.lx.bird.fly.browser.server.http.header.HttpHeader;
import org.lx.bird.fly.browser.server.http.requestline.HttpRequestLine;

/**
 * http报文的request的肢解的数据实体
 * Created by liuxu on 17-10-6.
 */
public class ZxxRequest {
    private HttpHeader httpHeader;
    private HttpRequestLine httpRequestLine;
    private String httpMessage;
    private String body;

    public HttpHeader getHttpHeader() {
        return httpHeader;
    }

    public void setHttpHeader(HttpHeader httpHeader) {
        this.httpHeader = httpHeader;
    }

    public HttpRequestLine getHttpRequestLine() {
        return httpRequestLine;
    }

    public void setHttpRequestLine(HttpRequestLine httpRequestLine) {
        this.httpRequestLine = httpRequestLine;
    }

    public String getHttpMessage() {
        return httpMessage;
    }

    public void setHttpMessage(String httpMessage) {
        this.httpMessage = httpMessage;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    /**
     * 编制http报文的类别：
     * 1-只含有请求行和头部信息
     * 2-请求航，头部信息，还有数据部分
     */
    public static enum HttpMessageKind {
        NO_BODY(1),
        HAVE_BODY(2);
        private int kindType;

        HttpMessageKind(int kindType) {
            this.kindType = kindType;
        }

        public int getKindType() {
            return this.kindType;
        }
    }
}
