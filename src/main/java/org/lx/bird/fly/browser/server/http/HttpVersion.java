package org.lx.bird.fly.browser.server.http;

/**
 * Http的版本枚举
 */
public enum HttpVersion {
    UN_KNOW_VERSION("0",0,"UN_KNOW/0.0"),
    HTTP_0_9("0.9", 1, "HTTP/0.9"),
    HTTP_1_0("1.0", 2, "HTTP/1.0"),
    HTTP_1_1("1.1", 3, "HTTP/1.1"),
    HTTP_2("2", 4, "HTTP/2");
    private String version;
    private int code;
    private String versionName;

    HttpVersion(String version, int code, String versionName) {
        this.version = version;
        this.code = code;
        this.versionName = versionName;
    }

    public String getVersion() {
        return version;
    }

    public int getCode() {
        return code;
    }

    public String getVersionName() {
        return versionName;
    }

    public static HttpVersion matchVersion(String versionInfo){
        HttpVersion[] values = HttpVersion.values();
        for (HttpVersion value : values) {
            if(value.getVersion().equalsIgnoreCase(versionInfo)||value.getVersionName().equalsIgnoreCase(versionInfo)){
                return value;
            }
        }
        return UN_KNOW_VERSION;
    }
}
