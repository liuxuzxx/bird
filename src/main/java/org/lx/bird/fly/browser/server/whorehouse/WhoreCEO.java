package org.lx.bird.fly.browser.server.whorehouse;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Date;
import java.util.Iterator;

import org.lx.bird.fly.browser.server.http.ZxxRequest;
import org.lx.bird.fly.browser.server.http.parser.HttpParser;
import org.lx.bird.fly.browser.server.http.parser.impl.Http11Parser;
import org.lx.bird.fly.browser.server.io.HttpReader;
import org.lx.bird.fly.browser.server.io.impl.FileHttpReader;
import org.lx.bird.fly.browser.server.utils.HttpIOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 妓院的CEO，就是老鸨的意思，主要的目的就是为了进行接受客户端的请求，然后
 * 交给下面的妓女处理客人,我感觉我的命名非常的正确
 * Created by liuxu on 17-9-20.
 */
public class WhoreCEO {

    private ServerSocketChannel server;
    private Selector selector;
    private int port;
    private ByteBuffer buffer = ByteBuffer.allocate(1024 * 1024);
    private ByteBuffer headerBuffer = ByteBuffer.allocate(1024 * 1024);
    private static Logger logger = LoggerFactory.getLogger(WhoreCEO.class);
    private HttpParser httpParser;
    private HttpReader httpReader = new FileHttpReader("/home/user/");
    private ZxxRequest request;

    public WhoreCEO() {
        System.out.println("Whore已经启动，开始接客了...");
        port = 12306;
    }

    public WhoreCEO(int port) {
        this();
        this.port = port;
    }

    public void start() throws IOException {
        init();
        service();
    }

    private void init() throws IOException {
        server = ServerSocketChannel.open().bind(new InetSocketAddress(port));
        logger.debug("初始化端口号为：" + port);
        server.configureBlocking(false);
        selector = Selector.open();
        server.register(selector, SelectionKey.OP_ACCEPT);
        httpParser = new Http11Parser();
    }

    private void service() throws IOException {
        while (true) {
            int count = selector.select(10);
            if (count <= 0) {
                continue;
            }

            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey next = iterator.next();
                iterator.remove();
                try {
                    if (next.isAcceptable()) {
                        ServerSocketChannel serverChannel = (ServerSocketChannel) next.channel();
                        SocketChannel client = serverChannel.accept();
                        client.configureBlocking(false);
                        client.register(selector, SelectionKey.OP_READ);
                    }
                    if (next.isReadable()) {
                        long sum = 0L;
                        SocketChannel channel = (SocketChannel) next.channel();
                        int number = channel.read(headerBuffer);
                        HttpIOUtils.writeAppend("/home/user/file.info", new String(headerBuffer.array(), 0, number));
                        while (number > 0) {
                            buffer.clear();
                            number = channel.read(buffer);
                            sum = +number;
                            HttpIOUtils.writeAppend("/home/user/file.info", new String(buffer.array(), 0, number));
                            logger.debug("朝向文件当中追加http的请求信息...请求到的字节数为：" + number);
                        }
                        logger.debug("总共接收到的字节数是：" + sum);
                        byte[] headerInfo = headerBuffer.array();
                        request = httpParser.parse(new String(headerInfo, 0, headerBuffer.position()));
                        channel.register(selector, SelectionKey.OP_WRITE);
                        buffer.clear();
                    }
                    if (next.isWritable()) {
                        System.out.println("回应客户端的请求..." + new Date());
                        ByteBuffer clientBuffer = ByteBuffer.allocate(1024 * 1024);
                        clientBuffer.put(HttpServer.HTTP_RESPONSE_HEAD.getBytes());
                        clientBuffer.put(responseJSON());
                        SocketChannel channel = (SocketChannel) next.channel();
                        clientBuffer.flip();
                        channel.write(clientBuffer);
                        channel.close();
                    }
                } catch (IOException ioEx) {
                    ioEx.printStackTrace();
                    next.cancel();
                    try {
                        next.channel().close();
                    } catch (IOException ex) {
                    }
                }
            }
        }
    }

    private byte[] responseJSON() throws IOException {
        return httpReader.read(/*request.getHttpRequestLine().getResourceUrl()*/"/fileupload.html");
    }
}
