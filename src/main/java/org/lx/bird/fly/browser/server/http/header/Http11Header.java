package org.lx.bird.fly.browser.server.http.header;

import java.util.HashMap;
import java.util.Map;

import org.lx.bird.fly.browser.server.http.HttpConstant;
import org.lx.bird.fly.browser.server.http.HttpVersion;

/**
 * http/1.1协议版本的头部实现
 * Created by liuxu on 2017-09-29.
 */
public class Http11Header implements HttpHeader {

    private static final HttpVersion VERSION = HttpVersion.HTTP_1_1;
    private Map<String, String> headers;

    public Http11Header(String headerInfo) {
        headers = new HashMap<>();
        parseHeader(headerInfo);
    }

    private void parseHeader(String headerInfo) {
        String[] headerArrays = headerInfo.split(HttpConstant.HTTP_MESSAGE_LINE_SPLIT);
        for (String headerEntity : headerArrays) {
            String[] headerMap = headerEntity.split(HttpConstant.HTTP_MESSAGE_HEADER_SPLIT, HEADER_SPLIT_COUNT);
            headers.put(headerMap[0].trim(), headerMap[1].trim());
        }
    }

    @Override
    public String header() {
        StringBuilder headerBuilder = new StringBuilder();
        headers.forEach((key, value) ->{
            headerBuilder.append(key);
            headerBuilder.append(": ");
            headerBuilder.append(value);
            headerBuilder.append(HttpConstant.HTTP_MESSAGE_LINE_SPLIT);
        } );
        headerBuilder.append(HttpConstant.HTTP_MESSAGE_LINE_SPLIT);
        return headerBuilder.toString();
    }

    @Override
    public Map<String, String> headersMap() {
        return headers;
    }


}
