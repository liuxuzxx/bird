package org.lx.bird.fly.browser.server.io;

import java.io.IOException;

/**
 * http读取资源的接口
 * Created by liuxu on 2017-10-10.
 */
public interface HttpReader {
    /**
     * 根据一个路径获取byte的数组
     * @param path 路径【文件路径，网络路径等等】
     * @return byte数组数据
     */
    byte[] read(String path) throws IOException;
}
