package org.lx.bird.fly.browser.server.whorehouse;

/**
 * 提供http服务的服务，我怀疑就是返回的头部信息导致解析的错误，毕竟这个是http的协议
 * Created by liuxu on 17-9-27.
 */
public class HttpServer {
    public static final String HTTP_RESPONSE_HEAD = "HTTP/1.1 200 OK yes\r\n" +
            "Server: LiuXu-NoBlockingHttpServer\r\n" +
            "Set-Cookie: jsessionid=qwert; userName=liuxu; password=lxzxx\r\n" +
            "Content-type: text/html\r\n" +
            "Content-length: 264900\r\n\r\n";

}
