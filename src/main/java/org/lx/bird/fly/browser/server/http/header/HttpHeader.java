package org.lx.bird.fly.browser.server.http.header;

import java.util.Map;

/**
 * http的头部信息的接口，设置信息，并且返回header的头部字符串
 * Created by liuxu on 2017-09-29.
 */
public interface HttpHeader {
    /**
     * http的一行头部信息分割的个数
     */
    public static final int HEADER_SPLIT_COUNT = 2;
    /**
     * 返回整体的header的信息字符串
     * @return header整体信息字符串
     */
    String header();

    /**
     * 返回http的header的map结构的数据形式，便于遍历操作
     *
     * @return header的map数据结构
     */
    Map<String, String> headersMap();
}
