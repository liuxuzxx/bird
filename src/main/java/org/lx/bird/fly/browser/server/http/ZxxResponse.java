package org.lx.bird.fly.browser.server.http;

import org.lx.bird.fly.browser.server.http.header.HttpHeader;
import org.lx.bird.fly.browser.server.http.requestline.HttpResponseLine;

/**
 * http协议的恢复的信息对象
 * Created by liuxu on 2017-10-20.
 */
public class ZxxResponse {
    private HttpResponseLine responseLine;
    private HttpHeader header;

    public String responseHeader(){
        StringBuilder builder = new StringBuilder();
        builder.append(responseLine);
        builder.append(header.header());
        return builder.toString();
    }
}
