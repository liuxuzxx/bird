package org.lx.bird.fly.browser.server.http.parser;

import org.lx.bird.fly.browser.server.http.ZxxRequest;

/**
 * 解析http协议的请求
 * Created by liuxu on 2017-09-30.
 */
public interface HttpParser {

    /**
     * 解析http的报文信息，形成ZxxRequest对象
     * @param httpMessage http的报文信息
     * @return ZxxRequest的http报文请求信息对象
     */
    ZxxRequest parse(String httpMessage);
}
