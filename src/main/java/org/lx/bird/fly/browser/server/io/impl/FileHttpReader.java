package org.lx.bird.fly.browser.server.io.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.lx.bird.fly.browser.server.io.HttpReader;

/**
 * 读取文件的http的实现
 * Created by liuxu on 2017-10-10.
 */
public class FileHttpReader implements HttpReader {

    private String basicFilePath;

    public FileHttpReader(String basicFilePath) {
        this.basicFilePath = basicFilePath;
    }

    @Override
    public byte[] read(String path) throws IOException {
        return Files.readAllBytes(Paths.get(washPath(path)));
    }

    /**
     * 清洗文件路径，因为传递的路径可能是不够和谐和友好的
     *
     * @param path 文件路径
     * @return 清洗过之后的路径信息
     */
    private String washPath(String path) {
        if (basicFilePath.endsWith("/")) {
            return basicFilePath.substring(0, basicFilePath.length() - 1) + path;
        } else {
            return basicFilePath + path;
        }
    }
}
