package org.lx.bird.fly.browser.server.http;

/**
 * 关于Http的一些常量的定义，已经是超越单个部分了，而是整个http的常量
 * Created by liuxu on 2017-09-30.
 */
public final class HttpConstant {

    /**
     * Http协议的一行内的解析使用空格，行与行之间使用\r\n进行分割,http的报文和内容之间是\r\n\r\n
     */
    public static final String HTTP_MESSAGE_REQUEST_SPLIT = " ";
    public static final String HTTP_MESSAGE_LINE_SPLIT = "\r\n";
    public static final String HTTP_MESSAGE_PART_SPLIT = "\r\n\r\n";
    /**
     * http协议的头部单行的信息分割
     */
    public static final String HTTP_MESSAGE_HEADER_SPLIT = ":";

    private HttpConstant() {
        throw new RuntimeException("不能制造一个常量类的对象");
    }
}
