package org.lx.bird.fly.browser.server.http.body;

/**
 * http协议的body解析接口
 * Created by liuxu on 17-10-9.
 */
public interface HttpBodyHandler {
    /**
     * 解析body的字节数据
     * @param bodies body的字节数组
     */
    void handler(byte[] bodies);
}
