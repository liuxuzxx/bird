package org.lx.bird.fly.browser.server.http.parser.impl;

import org.lx.bird.fly.browser.server.http.HttpConstant;
import org.lx.bird.fly.browser.server.http.ZxxRequest;
import org.lx.bird.fly.browser.server.http.body.HttpBodyHandler;
import org.lx.bird.fly.browser.server.http.body.impl.FileBodyHandler;
import org.lx.bird.fly.browser.server.http.header.Http11Header;
import org.lx.bird.fly.browser.server.http.header.HttpHeader;
import org.lx.bird.fly.browser.server.http.parser.HttpParser;
import org.lx.bird.fly.browser.server.http.requestline.HttpRequestLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * http协议1.1的请求解析.我认为这个是一个解剖器，既然是这样子
 * 那么，这个就是一个动作，我们需要找一个动作的数据承受体就行了
 * Created by liuxu on 2017-09-30.
 */
public class Http11Parser implements HttpParser {

    private static Logger logger = LoggerFactory.getLogger(Http11Parser.class);
    private static final int NO_BODY = 1;
    private static final int HAVE_BODY = 2;
    private HttpBodyHandler bodyHandler = new FileBodyHandler();

    /**
     * 开始肢解我们获取的报文
     *
     * @param httpMessage 等待被肢解的http的报文信息字符串
     */
    private ZxxRequest parseHttpMessage(String httpMessage) {
        logger.debug("开始肢解http的报文信息");
        String[] messages = httpMessage.split(HttpConstant.HTTP_MESSAGE_PART_SPLIT, 2);
        ZxxRequest request = new ZxxRequest();
        switch (messages.length) {
            case NO_BODY:
                logger.debug("没有body的数据部分");
                String[] bodies = messages[0].split(HttpConstant.HTTP_MESSAGE_LINE_SPLIT, 2);
                logger.debug("bodies的长度是："+bodies.length);
                request.setHttpRequestLine(parseRequestLine(bodies[0]));
                request.setHttpHeader(parseHeader(bodies[1]));
                break;
            case HAVE_BODY:
                logger.debug("含有body的数据部分");
                String[] haveBodies = messages[0].split(HttpConstant.HTTP_MESSAGE_LINE_SPLIT, 2);
                request.setHttpRequestLine(parseRequestLine(haveBodies[0]));
                request.setHttpHeader(parseHeader(haveBodies[1]));
                request.setBody(messages[1]);
                bodyHandler.handler(messages[1].getBytes());
                break;
            default:
                logger.debug("获取解析http的报文信息错误...");
                break;
        }
        request.setHttpMessage(httpMessage);
        logger.debug("肢解完成");
        return request;
    }

    @Override
    public ZxxRequest parse(String httpMessage) {
        return parseHttpMessage(httpMessage);
    }

    private HttpRequestLine parseRequestLine(String requestLine) {
        return new HttpRequestLine(requestLine);
    }

    private HttpHeader parseHeader(String headerContent) {
        return new Http11Header(headerContent);
    }

}
