package org.lx.bird.fly.browser.server;

import java.io.IOException;

import org.lx.bird.fly.browser.server.whorehouse.WhoreCEO;

/**
 * 服务端
 * Created by liuxu on 2017-09-27.
 */
public class Server {
    public static void main(String[] args) throws IOException {
        WhoreCEO whoreCEO = new WhoreCEO();
        whoreCEO.start();
    }
}
