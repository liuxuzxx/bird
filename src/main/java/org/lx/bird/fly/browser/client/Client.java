package org.lx.bird.fly.browser.client;

import org.lx.bird.fly.browser.client.customer.WhoreMaster;

import java.io.IOException;

/**
 * whoremaster客户端
 * Created by liuxu on 17-9-26.
 */
public class Client {

    public static void main(String[] args) throws IOException, InterruptedException {
        WhoreMaster whoreMaster = new WhoreMaster();
        whoreMaster.master();
    }
}
