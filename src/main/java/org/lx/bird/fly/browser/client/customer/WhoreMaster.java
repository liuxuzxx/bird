package org.lx.bird.fly.browser.client.customer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * whore-master客户
 * Created by liuxu on 17-9-26.
 */
public class WhoreMaster {

    public WhoreMaster() {
        System.out.println("whore master.....开始进入whore了....");
    }

    public void master() throws IOException, InterruptedException {
        init();
    }

    private void init() throws IOException, InterruptedException {
        while (true) {
            SocketChannel client = SocketChannel.open();
            Thread.sleep(1000);
            client.connect(new InetSocketAddress("127.0.0.1", 12306));
            write(client);
            client.close();
        }
    }

    private void write(SocketChannel client) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        buffer.put("我是一个少先队员...".getBytes());
        buffer.limit(buffer.position());
        buffer.position(0);
        client.write(buffer);
    }
}
