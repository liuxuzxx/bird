package org.lx.bird.fly.browser.server.http.requestline;

import com.alibaba.fastjson.JSONObject;
import org.lx.bird.fly.browser.server.http.HttpConstant;
import org.lx.bird.fly.browser.server.http.HttpMethod;
import org.lx.bird.fly.browser.server.http.HttpVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * http请求的请求行部分实体对象
 * Created by liuxu on 2017-09-30.
 */
public class HttpRequestLine {
    private HttpMethod method;
    private HttpVersion version;
    private String resourceUrl;

    public HttpMethod getMethod() {
        return method;
    }

    public HttpVersion getVersion() {
        return version;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    private static Logger logger = LoggerFactory.getLogger(HttpRequestLine.class);

    public HttpRequestLine(String requestLine) {
        parserRequestLine(requestLine);
    }

    private void parserRequestLine(String requestLine) {
        String trimRequestLine = requestLine.trim();
        String[] lineInfo = trimRequestLine.split(HttpConstant.HTTP_MESSAGE_REQUEST_SPLIT);

        if(lineInfo.length!=3){
            logger.debug("请求的http报文信息为："+ JSONObject.toJSONString(lineInfo));
            if(logger.isWarnEnabled()){
                logger.warn("Http请求行的数据不是三个部分，不能进行直接赋值操作，需要另外一种解析.不过目前，服务器不支持");
            }
        }else{
            method = HttpMethod.matchMethod(lineInfo[0]);
            resourceUrl = lineInfo[1];
            version = HttpVersion.matchVersion(lineInfo[2]);
        }
    }

}
