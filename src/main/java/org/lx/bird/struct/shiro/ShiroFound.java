package org.lx.bird.struct.shiro;

import java.util.Arrays;
import java.util.Collection;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Shiro安全框架使用探路者号
 *
 * @Author 刘旭
 * @Date 8:57 2018/6/1
 */
public class ShiroFound {
    private static final Logger logger = LoggerFactory.getLogger(ShiroFound.class);

    @Test
    public void authenticationTest() {
        Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");
        SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);

        Subject currentUser = SecurityUtils.getSubject();
        Session session = currentUser.getSession();
        session.setAttribute("userName", "rondo");
        session.setAttribute("authorization", "授权");

        Collection<Object> keys = session.getAttributeKeys();
        for (Object key : keys) {
            Object value = session.getAttribute(key);
            logger.info("会话存储的信息为：{},{}", key, value);
        }

        UsernamePasswordToken token = new UsernamePasswordToken("rajon", "rondo");
        token.setRememberMe(true);
        try {
            currentUser.login(token);
        }catch (UnknownAccountException unknownAccountException){
            logger.error(unknownAccountException.getMessage(),unknownAccountException);
            logger.error("账户名:{} 不存在",token.getUsername());
        }catch (AuthenticationException authenticationEx){
            logger.error(authenticationEx.getMessage(),authenticationEx);
        }

        Collection<Object> attributeKeys = currentUser.getSession().getAttributeKeys();
        for(Object key:attributeKeys){
            logger.info("登录的用户的session信息为：{} {}",key,currentUser.getSession().getAttribute(key));
        }

        if(currentUser.isAuthenticated()){
            logger.info("用户：{} 登录成功",currentUser.getPrincipal());
            logger.info("用户：{} 拥有角色：{}：{}",currentUser.getPrincipal(),"president",currentUser.hasRoles(Arrays.asList("president","clients")));
            currentUser.logout();
        }

    }
}
