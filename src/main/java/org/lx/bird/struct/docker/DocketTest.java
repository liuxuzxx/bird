package org.lx.bird.struct.docker;


import java.util.List;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Image;

/**
 * 使用java启动Docker的客户端实验
 * Created by liuxu on 2017-09-26.
 */
public class DocketTest {

    public static void main(String[] args) throws DockerCertificateException, DockerException, InterruptedException {
        final DefaultDockerClient docker = DefaultDockerClient.fromEnv()
            .build();
        //docker.pull("nx1.roaddb.ygomi.com:8889/rtv-tools:v3");

        List<Image> images = docker.listImages();
        System.out.println(images.size());
    }

}
