package org.lx.bird.struct.activiti.spring;

import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.ProcessEngineImpl;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.lx.bird.struct.activiti.original.WorkEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * activiti的引擎
 * Created by liuxu on 2017-11-07.
 */
public class ActivitiEngine {

    private static final String CONFIG_SPRING = "org/lx/bird/struct/activiti/spring/activiti-spring.xml";
    private static final Logger logger = LoggerFactory.getLogger(WorkEngine.class);
    private static final String PROCESS_ENGINE = "processEngine";
    private static final String SALES_CONFIG = "org/lx/bird/struct/activiti/flowxml/Sales.bpmn20.xml";
    private static final String FINANCIAL_REPORT_CONFIG = "org/lx/bird/struct/activiti/flowxml/FinancialReport.bpmn20.xml";

    /**
     * 载入工作流的配置文件
     * 当载入配置文件的时候，也是可以创建数据库
     */
    @Test
    public void loadConfig() {
        ApplicationContext context = new ClassPathXmlApplicationContext(CONFIG_SPRING);
        ProcessEngineImpl engine = context.getBean(PROCESS_ENGINE, ProcessEngineImpl.class);
        RuntimeService runtimeService = engine.getRuntimeService();
    }

    /**
     * 一个完整的实例
     */
    @Test
    public void tutorial() {
        // Create Activiti process engine
        ApplicationContext context = new ClassPathXmlApplicationContext(CONFIG_SPRING);
        ProcessEngineImpl processEngine = context.getBean(PROCESS_ENGINE, ProcessEngineImpl.class);
        // Get Activiti services
        RepositoryService repositoryService = processEngine.getRepositoryService();
        RuntimeService runtimeService = processEngine.getRuntimeService();

        // Deploy the process definition
        repositoryService.createDeployment()
            .addClasspathResource(SALES_CONFIG)
            .deploy();

        // Start a process instance
        String procId = runtimeService.startProcessInstanceByKey("financialReport").getId();

        // Get the first task
        TaskService taskService = processEngine.getTaskService();
        List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup("accountancy").list();
        for (Task task : tasks) {
            System.out.println("Following task is available for accountancy group: " + task.getName());

            // claim it
            taskService.claim(task.getId(), "fozzie");
        }

        // Verify Fozzie can now retrieve the task
        tasks = taskService.createTaskQuery().taskAssignee("fozzie").list();
        for (Task task : tasks) {
            System.out.println("Task for fozzie: " + task.getName());

            // Complete the task
            taskService.complete(task.getId());
        }

        System.out.println("Number of tasks for fozzie: "
            + taskService.createTaskQuery().taskAssignee("fozzie").count());

        // Retrieve and claim the second task
        tasks = taskService.createTaskQuery().taskCandidateGroup("management").list();
        for (Task task : tasks) {
            System.out.println("Following task is available for accountancy group: " + task.getName());
            taskService.claim(task.getId(), "kermit");
        }

        // Completing the second task ends the process
        for (Task task : tasks) {
            taskService.complete(task.getId());
        }

        // verify that the process is actually finished
        HistoryService historyService = processEngine.getHistoryService();
        HistoricProcessInstance historicProcessInstance =
            historyService.createHistoricProcessInstanceQuery().processInstanceId(procId).singleResult();
        System.out.println("Process instance end time: " + historicProcessInstance.getEndTime());
    }

    /**
     * 海盗行动
     */
    @Test
    public void pirate() {
        ApplicationContext context = new ClassPathXmlApplicationContext(CONFIG_SPRING);
        ProcessEngineImpl processEngine = context.getBean(PROCESS_ENGINE, ProcessEngineImpl.class);

        RepositoryService repositoryService = processEngine.getRepositoryService();
        repositoryService.createDeployment()
            .addClasspathResource(FINANCIAL_REPORT_CONFIG)
            .deploy();

        RuntimeService runtimeService = processEngine.getRuntimeService();
        long start = System.currentTimeMillis();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("financialReport");
        System.out.println("processInstance Id:" + processInstance.getProcessInstanceId());
        long end = System.currentTimeMillis();
        System.out.println("时间为：" + (end - start) / 1000);
    }

    @Test
    public void userTask(){
        ApplicationContext context = new ClassPathXmlApplicationContext(CONFIG_SPRING);
        ProcessEngineImpl processEngine = context.getBean(PROCESS_ENGINE, ProcessEngineImpl.class);

        RepositoryService repositoryService = processEngine.getRepositoryService();
        repositoryService.createDeployment()
                .addClasspathResource(SALES_CONFIG)
                .deploy();

        RuntimeService runtimeService = processEngine.getRuntimeService();
        long start = System.currentTimeMillis();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("userTask");
        System.out.println("processInstance Id:" + processInstance.getProcessInstanceId());
        long end = System.currentTimeMillis();
        System.out.println("时间为：" + (end - start) / 1000);
    }
}
