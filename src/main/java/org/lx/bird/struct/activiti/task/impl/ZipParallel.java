package org.lx.bird.struct.activiti.task.impl;

import org.activiti.engine.delegate.DelegateExecution;
import org.lx.bird.struct.activiti.task.ParallelTask;

/**
 * Created by liuxu on 2017-11-24.
 */
public class ZipParallel implements ParallelTask {

    @Override
    public void threadInfo(DelegateExecution execution) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("压缩的任务的task:" + Thread.currentThread().getId());
    }
}
