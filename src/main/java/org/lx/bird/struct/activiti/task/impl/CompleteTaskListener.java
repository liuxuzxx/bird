package org.lx.bird.struct.activiti.task.impl;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

/**
 * 查看Task的Complete的阶段
 * Create by xu.liu on 2018-03-05
 */
public class CompleteTaskListener implements TaskListener{
    @Override
    public void notify(DelegateTask delegateTask) {
        System.out.println("Complete阶段的监听器");
    }
}
