package org.lx.bird.struct.activiti.task;

import org.activiti.engine.delegate.DelegateExecution;

/**
 * 生命流程开始的事件
 * Created by liuxu on 2018-01-05.
 */
public interface LifeListener {
    /**
     * 执行开始的事件
     * @param execution 代理执行事物体
     */
    void listener(DelegateExecution execution);
}
