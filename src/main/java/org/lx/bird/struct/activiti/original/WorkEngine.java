package org.lx.bird.struct.activiti.original;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.DynamicBpmnService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 工作引擎实验工作
 * Created by liuxu on 2017-11-07.
 */
public class WorkEngine {

    private static final Logger logger = LoggerFactory.getLogger(WorkEngine.class);

    /**
     * 载入工作流的配置文件
     * 当载入配置文件的时候，也是可以创建数据库
     */
    @Test
    public void loadConfig() {
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
    }

    /**
     * 查看engine和他提供的service
     * 我们最终需要的是service，不是engine，但是engine提供了原动力
     */
    @Test
    public void engineAndService() {
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = engine.getRuntimeService();
        RepositoryService repositoryService = engine.getRepositoryService();
        TaskService taskService = engine.getTaskService();
        ManagementService managementService = engine.getManagementService();
        HistoryService historyService = engine.getHistoryService();
        DynamicBpmnService dynamicBpmnService = engine.getDynamicBpmnService();
    }

    /**
     * 测试部署，暂且不知道部署的作用是干嘛的
     */
    @Test
    public void deploy() {
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = engine.getRepositoryService();
        repositoryService.createDeployment()
            .addClasspathResource("org/lx/bird/struct/activiti/flowxml/VacationRequest.bpmn20.xml")
            .deploy();
        logger.debug(MessageFormat.format("定义的processor的数量是：{0}", repositoryService.createDeploymentQuery().count()));
    }

    /**
     * 执行对应的流程实例。就是流程定义的具体化
     */
    @Test
    public void process() {
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = engine.getRepositoryService();
        repositoryService.createDeployment()
            .addClasspathResource("org/lx/bird/struct/activiti/flowxml/VacationRequest.bpmn20.xml")
            .deploy();

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("employeeName", "Kermit");
        variables.put("numberOfDays", new Integer(4));
        variables.put("vacationMotivation", "我真的努力了...");
        RuntimeService runtimeService = engine.getRuntimeService();
        runtimeService.startProcessInstanceByKey("vacationRequest", variables);
        logger.debug(MessageFormat.format("产生的流程个数是：{0}", runtimeService.createProcessInstanceQuery().count()));
    }

    /**
     * 完成对应的任务
     */
    @Test
    public void complete(){
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = engine.getRepositoryService();
        repositoryService.createDeployment()
            .addClasspathResource("org/lx/bird/struct/activiti/flowxml/VacationRequest.bpmn20.xml")
            .deploy();
        TaskService taskService = engine.getTaskService();
        List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup("management").list();
        for (Task task : tasks) {
            logger.info("可利用的Task: " + task.getName());
        }

        Task task = tasks.get(0);

        Map<String, Object> taskVariables = new HashMap<>();
        taskVariables.put("vacationApproved", "false");
        taskVariables.put("managerMotivation", "We have a tight deadline!");
        taskService.complete(task.getId(), taskVariables);
    }

    /**
     * 测试挂起任务
     */
    @Test
    public void suspend(){
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = engine.getRepositoryService();
        repositoryService.suspendProcessDefinitionByKey("vacationRequest");

        RuntimeService runtimeService = engine.getRuntimeService();
        try {
            runtimeService.startProcessInstanceByKey("vacationRequest");
        } catch (ActivitiException e) {
            e.printStackTrace();
        }

    }
}
