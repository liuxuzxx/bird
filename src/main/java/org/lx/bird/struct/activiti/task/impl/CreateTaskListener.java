package org.lx.bird.struct.activiti.task.impl;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

/**
 * 实现一个监听任务的生命周期监听器
 * Create by xu.liu on 2018-03-05
 */
public class CreateTaskListener implements TaskListener{

    @Override
    public void notify(DelegateTask delegateTask) {
        System.out.println("执行create阶段的操作");
    }
}
