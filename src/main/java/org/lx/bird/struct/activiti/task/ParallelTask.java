package org.lx.bird.struct.activiti.task;

import org.activiti.engine.delegate.DelegateExecution;

/**
 * 并行任务接口
 * Created by liuxu on 2017-11-24.
 */
public interface ParallelTask {

    void threadInfo(DelegateExecution execution);
}
