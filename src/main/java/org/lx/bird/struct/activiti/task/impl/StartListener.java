package org.lx.bird.struct.activiti.task.impl;

import org.activiti.engine.delegate.DelegateExecution;
import org.lx.bird.struct.activiti.task.LifeListener;

/**
 * 开始事件的执行
 * Created by liuxu on 2018-01-05.
 */
public class StartListener implements LifeListener {

    @Override
    public void listener(DelegateExecution execution) {
        System.out.println("开始执行的事件：" + execution.getProcessInstanceId());
    }
}
