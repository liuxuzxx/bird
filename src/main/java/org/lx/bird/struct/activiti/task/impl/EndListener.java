package org.lx.bird.struct.activiti.task.impl;

import org.activiti.engine.delegate.DelegateExecution;
import org.lx.bird.struct.activiti.task.LifeListener;

/**
 * 结束事件的监听器
 * Created by liuxu on 2018-01-05.
 */
public class EndListener implements LifeListener{

    @Override
    public void listener(DelegateExecution execution) {
        System.out.println("end事件："+execution.getProcessInstanceId());
    }
}
