package org.lx.bird.struct.thrift.server.service;

import org.apache.thrift.TException;
import org.lx.bird.struct.thrift.gen.HelloService;

/**
 * 实现Hello的接口服务
 * Created by liuxu on 18-4-21.
 */
public class HelloServiceImpl implements HelloService.Iface{
    @Override
    public String welcome(String userName) throws TException {
        return "您好："+userName;
    }
}
