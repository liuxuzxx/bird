package org.lx.bird.struct.thrift.server;

import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;
import org.lx.bird.struct.thrift.gen.HelloService;
import org.lx.bird.struct.thrift.server.service.HelloServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 提供thrift的服务端的接口使用
 * Created by liuxu on 18-4-21.
 */
public class ThriftServer {
    private static final Logger logger = LoggerFactory.getLogger(ThriftServer.class);
    private static final int THRIFT_SERVER_PORT = 9600;

    public static void main(String[] args){
        ThriftServer thriftServer = new ThriftServer();
        thriftServer.start();
    }

    private void start(){
        logger.debug("Thrift的Server端开始启动...");
        try {

            HelloService.Processor<HelloService.Iface> helloProcessor = new HelloService.Processor<>(new HelloServiceImpl());

            TServerSocket serverSocket = new TServerSocket(THRIFT_SERVER_PORT);
            TServer.Args args = new TServer.Args(serverSocket);
            args.processor(helloProcessor);
            args.protocolFactory(new TCompactProtocol.Factory());

            TSimpleServer server = new TSimpleServer(args);
            server.serve();
        }catch (Exception ex){
            logger.error("Thrift的Server启动失败",ex);
        }
    }
}
