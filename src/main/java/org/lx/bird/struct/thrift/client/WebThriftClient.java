package org.lx.bird.struct.thrift.client;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.lx.bird.struct.thrift.gen.HelloService;

/**
 * 模拟Web的Thrift的客户端
 * Created by liuxu on 18-4-21.
 */
public class WebThriftClient {

    private static final String SERVER_HOST = "localhost";
    private static final int SERVER_PORT = 9600;
    private static final int TIMEOUT = 30000;

    public static void main(String[] args) throws TException {
        WebThriftClient thriftClient = new WebThriftClient();
        thriftClient.start();
    }

    private void start() throws TException {
        TTransport transport = new TSocket(SERVER_HOST, SERVER_PORT, TIMEOUT);
        TProtocol protocol = new TCompactProtocol(transport);
        HelloService.Client client = new HelloService.Client(protocol);
        transport.open();
        String result = client.welcome("柳絮");
        System.out.println(result);
    }
}
