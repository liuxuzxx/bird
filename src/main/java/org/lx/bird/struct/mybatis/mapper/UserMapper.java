package org.lx.bird.struct.mybatis.mapper;

import java.util.List;
import java.util.Map;

import org.lx.bird.struct.mybatis.dto.UserDTO;

/**
 * 一个Role的Mapper
 * Created by liuxu on 7/21/17.
 */
public interface UserMapper {
    List<UserDTO> getAllUserInfos();

    UserDTO getUserById(int userId);

    UserDTO getUserByIdAndUserName(Map<String,Object> param);

    UserDTO findUser(Map<String,Object> param);
}
