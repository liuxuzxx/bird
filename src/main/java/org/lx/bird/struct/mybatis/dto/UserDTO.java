package org.lx.bird.struct.mybatis.dto;

import java.util.Date;

/**
 * 一个dto的user
 * Created by liuxu on 2017-08-18.
 */
public class UserDTO {
    private int userId;
    private String userName;
    private Date birthday;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
