package org.lx.bird.struct.mybatis.founder;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import org.lx.bird.struct.mybatis.dto.UserDTO;
import org.lx.bird.struct.mybatis.mapper.UserMapper;

import com.alibaba.fastjson.JSONObject;

/**
 * Mybatis的founder探索号
 * Created by liuxu on 2017-08-18.
 */
public class MybatisFounder {
    private static final String MYBATIS_CONFIG = "mybatis-config.xml";
    private static SqlSessionFactory build;
    private static SqlSession sqlSession ;

    static{
        try {
            build = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader(MYBATIS_CONFIG));
            sqlSession = build.openSession();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试传递map参数怎么进行sql的处理赋值
     */
    @Test
    public void mapParam(){
        Map<String, Object> param = new HashMap<>();
        param.put("userId",1);
        param.put("userName","lx");
        UserMapper roleMapper = sqlSession.getMapper(UserMapper.class);
        UserDTO user = roleMapper.getUserByIdAndUserName(param);
        System.out.println(JSONObject.toJSONString(user));
    }
}
