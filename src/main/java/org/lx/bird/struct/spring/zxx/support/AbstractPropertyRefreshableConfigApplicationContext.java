package org.lx.bird.struct.spring.zxx.support;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractRefreshableApplicationContext;

/**
 * 具有刷新可配置的Property的抽象的类
 * 这个类的主要目的就是：设置配置文件的路径信息
 * Created by liuxu on 17-10-11.
 */
public abstract class AbstractPropertyRefreshableConfigApplicationContext extends AbstractRefreshableApplicationContext
        implements BeanNameAware, InitializingBean {

    private String[] configurations;
    private static final String CONFIG_SPLIT = ";";
    private static Logger logger = LoggerFactory.getLogger(AbstractPropertyRefreshableConfigApplicationContext.class);

    /**
     * 给出来两个构造方法，一个是没有继承的，一个是有继承性质的
     */
    public AbstractPropertyRefreshableConfigApplicationContext() {
    }

    public AbstractPropertyRefreshableConfigApplicationContext(ApplicationContext parent) {
        super(parent);
    }

    public void setConfigLocation(String configLocation) {
        logger.debug("设置综合的配置文件信息");
        setConfigLocations(configLocation.split(CONFIG_SPLIT));
    }

    public void setConfigLocations(String... locations) {
        logger.debug("设置配置文件的路径信息：" + JSONObject.toJSONString(locations));
        if (locations != null) {
            configurations = locations;
        } else {
            configurations = null;
        }
    }

    protected String[] getConfigLocations() {
        logger.debug("获取配置文件的路径信息");
        return configurations;
    }

    @Override
    public void setBeanName(String name) {
        setDisplayName("liuxu property application context " + name);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.debug("加载配置信息之后，进行一个触发器的操作...");
    }

    @Override
    protected void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
        logger.debug("设置BeanFactory的后置处理，这种处理的函数好像都是void的内部消化分割代码函数");
    }
}
