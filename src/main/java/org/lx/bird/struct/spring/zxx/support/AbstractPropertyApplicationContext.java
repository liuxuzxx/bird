package org.lx.bird.struct.spring.zxx.support;

import java.io.IOException;

import org.lx.bird.struct.spring.zxx.reader.PropertyBeanDefinitionReader;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractRefreshableConfigApplicationContext;

/**
 * 一个抽象的读取配置文件形式组合成我们需要的bean的装配
 * 这是一个从抽象的配置文件到具体的配置文件的过程
 * 因为上一层级是：config的抽象的东西，那么，我们可以认为只要是实现这个config出现类的，肯定都是配置文件形式，
 * 但是从网络上读取的就不是这种形式了
 * Created by liuxu on 2017-09-08.
 */
public abstract class AbstractPropertyApplicationContext extends AbstractPropertyRefreshableConfigApplicationContext{
    public AbstractPropertyApplicationContext(){
    }

    public AbstractPropertyApplicationContext(ApplicationContext parent){
        super(parent);
    }

    /**
     * 从properties文件当中读取我们的信息内容
     */
    @Override
    protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory) throws BeansException, IOException {
        PropertyBeanDefinitionReader propertyBeanDefinitionReader = new PropertyBeanDefinitionReader(beanFactory);
        loadBeanDefinitions(propertyBeanDefinitionReader);
    }

    private void loadBeanDefinitions(PropertyBeanDefinitionReader propertyBeanDefinitionReader){
        int count = propertyBeanDefinitionReader.loadBeanDefinitions(getConfigLocations());
        System.out.println("从property文件当中加载了:" + count + " 个bean对象.");
    }
}
