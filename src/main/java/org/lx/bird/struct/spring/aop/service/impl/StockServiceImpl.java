package org.lx.bird.struct.spring.aop.service.impl;

import org.lx.bird.struct.spring.aop.service.StockService;

/**
 * 股票的买卖交易服务
 * Created by liuxu on 18-4-30.
 */
public class StockServiceImpl implements StockService {
    private volatile int count;

    @Override
    public void buy(int stockCount) {
        count += stockCount;
    }

    @Override
    public void sale(int stockCount) {
        count -= stockCount;
    }
}
