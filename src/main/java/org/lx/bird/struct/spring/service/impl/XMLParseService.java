package org.lx.bird.struct.spring.service.impl;

import org.lx.bird.javacore.dto.UserDto;
import org.lx.bird.struct.spring.service.ParseService;

/**
 * The XML handler service
 * Created by liuxu on 7/20/17.
 */
public class XMLParseService implements ParseService {
    private String name;
    private UserDto userDto;

    public XMLParseService() {
        System.out.println("xmlParser:使用了lazy-init参数，设置为true，开始初始化对象...");
    }

    public void setName(String name) {
        this.name = name;
        System.out.println("xmlParser设置了name属性为：" + name);
    }

    public void setUserDto(UserDto userDto){
        this.userDto = userDto;
    }

    @Override
    public String parseHead(String context) {
        return "xml:<attribute name='userName' value='zxx'></attribute>";
    }
}
