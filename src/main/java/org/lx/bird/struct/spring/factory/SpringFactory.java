package org.lx.bird.struct.spring.factory;

import org.junit.Test;
import org.lx.bird.struct.spring.constant.SpringConstant;
import org.lx.bird.struct.spring.service.impl.JSONParseService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试spring的factory
 * Created by liuxu on 7/20/17.
 */
public class SpringFactory {

    @Test
    public void beanFactory() {
        ApplicationContext context = new ClassPathXmlApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        JSONParseService jsonParse = context.getBean(JSONParseService.class);
        System.out.println(jsonParse.parseHead(""));
    }
}
