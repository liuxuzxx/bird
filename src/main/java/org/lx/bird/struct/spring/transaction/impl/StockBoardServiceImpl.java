package org.lx.bird.struct.spring.transaction.impl;

import org.lx.bird.struct.spring.transaction.StockBoardService;

/**
 * 股票交易的实现接口
 * Created by liuxu on 18-5-15.
 */
public class StockBoardServiceImpl implements StockBoardService {

    private int sum;

    @Override
    public int buy(int count) {
        sum += count;
        return sum;
    }

    @Override
    public void sale(int count) {
        sum -= count;
    }
}
