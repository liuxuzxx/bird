package org.lx.bird.struct.spring.context;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 使用initPropertySource方法的Override进行注入属性
 * Created by liuxu on 7/20/17.
 */
public class SelfApplicationContext extends ClassPathXmlApplicationContext {

    public SelfApplicationContext(String configuration) {
        super(configuration);
    }

    @Override
    protected void initPropertySources() {
        super.initPropertySources();
        System.out.println("start init the stub properties sources!");
    }
}