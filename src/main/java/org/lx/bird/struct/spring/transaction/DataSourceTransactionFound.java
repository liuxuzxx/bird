package org.lx.bird.struct.spring.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * 关于数据源的事务实验核基地
 * Created by liuxu on 18-5-15.
 */
public class DataSourceTransactionFound {
    private static final Logger logger = LoggerFactory.getLogger(DataSourceTransactionFound.class);
    private PlatformTransactionManager platformTransactionManager;
    private JdbcTemplate template;

    public void setPlatformTransactionManager(PlatformTransactionManager platformTransactionManager) {
        this.platformTransactionManager = platformTransactionManager;
    }

    public void setDataSource(DataSource dataSource) {
        template = new JdbcTemplate(dataSource);
    }

    public void defaultTransaction() {
        DefaultTransactionDefinition defaultTransactionDefinition = new DefaultTransactionDefinition();
        executeTransaction(defaultTransactionDefinition);
    }

    public void notSupportedTransaction(){
        DefaultTransactionDefinition definition = new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_NOT_SUPPORTED);
        executeTransaction(definition);
    }

    public void newTransaction(){
        DefaultTransactionDefinition definition = new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        executeTransaction(definition);
    }

    public void manDatoryTransaction(){
        DefaultTransactionDefinition definition = new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_MANDATORY);
        executeTransaction(definition);
    }

    private void executeTransaction(DefaultTransactionDefinition defaultTransactionDefinition) {
        TransactionStatus status = platformTransactionManager.getTransaction(defaultTransactionDefinition);
        try {
            String sql = "UPDATE user SET user.birthday=now()";
            template.execute(sql);
            throwException();
            platformTransactionManager.commit(status);
        }catch (Throwable thr){
            logger.error(thr.getMessage(),thr);
            platformTransactionManager.rollback(status);
        }
    }

    private void throwException() throws SQLException {
        throw new SQLException("扔出去一个异常信息看看...");
    }
}
