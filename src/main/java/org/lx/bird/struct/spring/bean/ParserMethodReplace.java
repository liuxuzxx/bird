package org.lx.bird.struct.spring.bean;

import org.springframework.beans.factory.support.MethodReplacer;

import java.lang.reflect.Method;

/**
 * 使用方法的替换工作
 * Created by liuxu on 17-11-9.
 */
public class ParserMethodReplace implements MethodReplacer {
    @Override
    public Object reimplement(Object obj, Method method, Object[] args) throws Throwable {
        System.out.println("替换xml的解析的实现方法：parseHead");
        return null;
    }
}
