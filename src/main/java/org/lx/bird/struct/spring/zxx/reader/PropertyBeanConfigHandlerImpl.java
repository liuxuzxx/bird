package org.lx.bird.struct.spring.zxx.reader;

import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
/**
 * 实现从property文件中读取bean的配置信息，并且转化成BeanDefinition的List集合
 * Created by liuxu on 17-9-9.
 */
public class PropertyBeanConfigHandlerImpl implements PropertyBeanConfigHandler {
    private static final String BEAN_NAME = ".name";
    private static final String BEAN_CLASS = ".class";
    private static final String BEAN_SPLIT_FLAG = ".";

    private ClassLoader classLoader;

    public PropertyBeanConfigHandlerImpl(ClassLoader classLoader){
        this.classLoader = classLoader;
    }

    @Override
    public List<BeanDefinitionHolder> parseToBeanDefinitionHolder(Properties properties) {
        List<BeanDefinitionHolder> beanDefinitionHolders = new ArrayList<>();
        List<String> beanNames = parseBeanNames(properties);
        beanNames.forEach(beanName -> {
            String beanClass = properties.getProperty(beanName + BEAN_CLASS);
            try {
                AbstractBeanDefinition beanDefinition = BeanDefinitionReaderUtils.createBeanDefinition(null, beanClass, classLoader);
                beanDefinitionHolders.add(new BeanDefinitionHolder(beanDefinition,beanName,new String[]{}));
            } catch (ClassNotFoundException e) {
                System.out.println("注册bean失败,注册的bean的名字为:"+beanName);
            }
        });
        return beanDefinitionHolders;
    }

    private List<String> parseBeanNames(Properties properties) {
        Set<String> keys = properties.stringPropertyNames();
        return keys.stream()
                .map((String s) -> s.substring(0, s.indexOf(BEAN_SPLIT_FLAG)))
                .collect(Collectors.toList());
    }
}
