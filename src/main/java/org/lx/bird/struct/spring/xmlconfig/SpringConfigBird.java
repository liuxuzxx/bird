package org.lx.bird.struct.spring.xmlconfig;

import org.junit.Test;
import org.lx.bird.struct.spring.constant.SpringConstant;
import org.lx.bird.struct.spring.context.SelfApplicationContext;
import org.lx.bird.struct.spring.service.impl.HTMLParseService;

/**
 * The spring config file bird.
 * Created by liuxu on 7/21/17.
 */
public class SpringConfigBird {

    @Test
    public void importTest() {
        SelfApplicationContext context = new SelfApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        HTMLParseService htmlParse = context.getBean(HTMLParseService.class);
        System.out.println(htmlParse.parseHead(""));
    }
}
