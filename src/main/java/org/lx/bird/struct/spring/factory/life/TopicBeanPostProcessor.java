package org.lx.bird.struct.spring.factory.life;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * 通知形式的bean的后置处理
 * Created by liuxu on 2017-10-26.
 */
public class TopicBeanPostProcessor implements BeanPostProcessor {
    protected static Logger logger = LoggerFactory.getLogger(TopicBeanPostProcessor.class);

    public TopicBeanPostProcessor(){
        logger.debug("初始化这个topic的通知的处理动作...");
    }
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        logger.debug("Before后置处理动作的bean的名字是：" + beanName);
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        logger.debug("After后置处理处理动作的bean的名字是：" + beanName);
        return bean;
    }
}
