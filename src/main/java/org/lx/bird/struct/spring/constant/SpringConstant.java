package org.lx.bird.struct.spring.constant;

/**
 * 创建一个spring使用到的常量的东西
 * Created by liuxu on 7/20/17.
 */
public final class SpringConstant {

    public static final String SPRING_CONFIG_FILE_PATH = "./spring.xml";

    public static final String SPRING_CONFIG_PROPERTY_FILE_PATH = "spring-bean.properties";


    private SpringConstant(){
        throw new RuntimeException();
    }
}
