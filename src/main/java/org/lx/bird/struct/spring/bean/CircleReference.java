package org.lx.bird.struct.spring.bean;

/**
 * 测试循环依赖的问题
 * Created by liuxu on 2017-11-06.
 */
public class CircleReference {

}

class Boss {
    private CarDriver driver;

    public Boss() {
        driver = new CarDriver();
    }
}

class CarDriver {
    private Boss boss;

    public CarDriver() {
        boss = new Boss();
    }
}
