package org.lx.bird.struct.spring.service;

/**
 * Create a interface to test the spring bean factory
 * Created by liuxu on 7/20/17.
 */
public interface ParseService {

    String parseHead(String context);
}
