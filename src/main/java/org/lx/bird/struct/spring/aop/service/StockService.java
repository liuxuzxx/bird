package org.lx.bird.struct.spring.aop.service;

/**
 * 股票服务
 * Created by liuxu on 18-4-30.
 */
public interface StockService {

    void buy(int stockCount);

    void sale(int stockCount);
}
