package org.lx.bird.struct.spring.transaction;

/**
 * 进行股票交易的接口
 * Created by liuxu on 18-5-15.
 */
public interface StockBoardService {

    int buy(int count);

    void sale(int count);
}
