package org.lx.bird.struct.spring.service.impl;

import org.lx.bird.struct.spring.service.ParseService;

/**
 * The HTML context to handler.
 * Created by liuxu on 7/20/17.
 */
public class HTMLParseService implements ParseService {
    private String name;

    public HTMLParseService() {
        System.out.println("没有设置lazy-init参数，那么默认是false，没有进行对象的初始化操作...");
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String parseHead(String context) {
        return "<head><title>The Spring Bean</title></head>";
    }
}
