package org.lx.bird.struct.spring.transaction;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * 自己编程控制数据库的事务
 * Create by xu.liu on 2018-05-10
 */
public class ProgramTransaction {

    private DataSource dataSource;
    private DataSourceTransactionManager txManager;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setTxManager(DataSourceTransactionManager txManager) {
        this.txManager = txManager;
    }

    public void lowLevel() {
        DefaultTransactionDefinition txDefinition = new DefaultTransactionDefinition();
        txDefinition.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        txDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(txDefinition);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            Statement statement = connection.createStatement();
            boolean flag = statement.execute("UPDATE RecordAttributeInfo SET attrName='修改' where categoryCode='road'");
            txManager.commit(status);
        }catch (SQLException ex){
            status.setRollbackOnly();
            txManager.rollback(status);
        }finally {
            DataSourceUtils.releaseConnection(connection,dataSource);
        }
    }
}
