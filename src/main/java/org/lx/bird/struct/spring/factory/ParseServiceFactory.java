package org.lx.bird.struct.spring.factory;

import org.lx.bird.struct.spring.service.ParseService;
import org.lx.bird.struct.spring.service.impl.HTMLParseService;
import org.springframework.beans.factory.FactoryBean;

/**
 * 实现一个bean的factory看看spring是怎么反应处理的
 * Created by liuxu on 17-11-8.
 */
public class ParseServiceFactory implements FactoryBean<ParseService> {
    @Override
    public ParseService getObject() throws Exception {
        return new HTMLParseService();
    }

    @Override
    public Class<?> getObjectType() {
        return ParseService.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
