package org.lx.bird.struct.spring.zxx.reader;

import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinitionReader;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 * 自己构造一个读取信息，之后生成BeanDefinition对象
 * Created by liuxu on 2017-09-08.
 */
public class PropertyBeanDefinitionReader extends AbstractBeanDefinitionReader {
    private static final String BEAN_NAME = "bean.name";
    private static final String BEAN_CLASS = "bean.class";
    private PropertyBeanConfigHandler propertyBeanConfigHandler;

    public PropertyBeanDefinitionReader(BeanDefinitionRegistry registry) {
        super(registry);
        propertyBeanConfigHandler = new PropertyBeanConfigHandlerImpl(this.getBeanClassLoader());
    }

    /**
     * 就是利用这个接口去读取资源当中的数据，然后转成我们需要的数据形式对象，其实也是包含了一种面向对象的编程，数据也是
     * 对象形式处理操作
     */
    @Override
    public int loadBeanDefinitions(Resource resource) throws BeanDefinitionStoreException {
        int start = getRegistry().getBeanDefinitionCount();
        try (InputStream inputStream = resource.getInputStream()) {
            Properties properties = new Properties();
            properties.load(inputStream);
            registryBeanDefinition(properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int end = getRegistry().getBeanDefinitionCount();
        return end - start;
    }

    private void registryBeanDefinition(Properties properties){
        List<BeanDefinitionHolder> beanDefinitionHolders = propertyBeanConfigHandler.parseToBeanDefinitionHolder(properties);
        beanDefinitionHolders.forEach(beanDefinitionHolder -> BeanDefinitionReaderUtils.registerBeanDefinition(beanDefinitionHolder,getRegistry()));
    }
}
