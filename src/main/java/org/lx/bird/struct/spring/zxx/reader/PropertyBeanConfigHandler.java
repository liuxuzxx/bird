package org.lx.bird.struct.spring.zxx.reader;

import org.springframework.beans.factory.config.BeanDefinitionHolder;

import java.util.List;
import java.util.Properties;

/**
 * 读取bean的配置的一个处理器
 * Created by liuxu on 17-9-9.
 */
public interface PropertyBeanConfigHandler {
    /**
     * 从properties文件当中获取bean的定义信息
     * @param properties
     * @return
     */
    List<BeanDefinitionHolder> parseToBeanDefinitionHolder(Properties properties);
}
