package org.lx.bird.struct.spring.bean;

/**
 * 实验spring进行一个类的override的实验工作
 * Created by liuxu on 17-11-9.
 */
public class VirtualMethod {

    public void loadData() {
        System.out.println(this.getClass().getName() + "从数据库load数据到内存当中来...");
    }
}
