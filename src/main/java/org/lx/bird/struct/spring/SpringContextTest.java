package org.lx.bird.struct.spring;

import org.junit.Test;
import org.lx.bird.struct.spring.aop.service.StockService;
import org.lx.bird.struct.spring.constant.SpringConstant;
import org.lx.bird.struct.spring.bean.VirtualMethod;
import org.lx.bird.struct.spring.context.SelfApplicationContext;
import org.lx.bird.struct.spring.service.ParseService;
import org.lx.bird.struct.spring.service.impl.HTMLParseService;
import org.lx.bird.struct.spring.service.impl.XMLParseService;
import org.lx.bird.struct.spring.transaction.DataSourceTransactionFound;
import org.lx.bird.struct.spring.transaction.ProgramTransaction;
import org.lx.bird.struct.spring.zxx.support.PropertyApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.SQLException;

/**
 * 构造一个Spring的上下文容器获取，便于我们操作和实验
 * Created by liuxu on 7/20/17.
 */
public class SpringContextTest {

    @Test
    public void gainContext() {
        ApplicationContext context = new ClassPathXmlApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        ParseService xmlParseService = context.getBean(XMLParseService.class);
        System.out.println(xmlParseService.parseHead("<></>"));
    }

    @Test
    public void factoryBean() {
        ApplicationContext context = new ClassPathXmlApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        ParseService bean = context.getBean("parseFactory", HTMLParseService.class);
        System.out.println(bean.parseHead("<>"));
    }

    @Test
    public void selfContext() {
        SelfApplicationContext context = new SelfApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        HTMLParseService htmlParse = context.getBean(HTMLParseService.class);
        System.out.println(htmlParse.parseHead("<html></html>"));
    }

    /**
     * 使用从Property的ApplicationContext进行实验
     */
    @Test
    public void propertyContext() {
        PropertyApplicationContext propertyApplicationContext = new PropertyApplicationContext(SpringConstant.SPRING_CONFIG_PROPERTY_FILE_PATH);
        XMLParseService bean = propertyApplicationContext.getBean(XMLParseService.class);
        System.out.println(bean.parseHead("<beans></beans>"));
    }

    /**
     * 测试spring的override的属性
     */
    @Test
    public void overrideMethod() {
        ApplicationContext context = new ClassPathXmlApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        VirtualMethod virtualMethod = context.getBean(VirtualMethod.class);
        virtualMethod.loadData();
    }

    /**
     * 日志的切面aop实验
     */
    @Test
    public void aspectLog() {
        ApplicationContext context = new ClassPathXmlApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        StockService stock = context.getBean(StockService.class);
        stock.buy(200);
        stock.sale(1000);
    }

    /**
     * 探究一下数据库的事务操作
     */
    @Test
    public void databaseDefaultTransaction() throws SQLException {
        ApplicationContext context = new ClassPathXmlApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        DataSourceTransactionFound found = context.getBean(DataSourceTransactionFound.class);
        found.defaultTransaction();
    }

    /**
     * 探究spring提供的数据库事务操作:not supported:就是当前事务挂起然后，以非事务的方式执行操作。
     */
    @Test
    public void databaseNotSupportedTransaction() {
        ApplicationContext context = new ClassPathXmlApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        DataSourceTransactionFound found = context.getBean(DataSourceTransactionFound.class);
        found.notSupportedTransaction();
    }

    @Test
    public void databaseNewTransaction() {
        ApplicationContext context = new ClassPathXmlApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        DataSourceTransactionFound found = context.getBean(DataSourceTransactionFound.class);
        found.newTransaction();
    }

    @Test
    public void databaseManDatory(){
        ApplicationContext context = new ClassPathXmlApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        DataSourceTransactionFound found = context.getBean(DataSourceTransactionFound.class);
        found.manDatoryTransaction();
    }
    @Test
    public void programTransaction(){
        ApplicationContext context = new ClassPathXmlApplicationContext(SpringConstant.SPRING_CONFIG_FILE_PATH);
        ProgramTransaction programTransaction = context.getBean(ProgramTransaction.class);
        programTransaction.lowLevel();
    }

}
