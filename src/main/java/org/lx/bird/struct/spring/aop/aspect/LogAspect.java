package org.lx.bird.struct.spring.aop.aspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 记录日志的切面。
 * Created by liuxu on 18-4-30.
 */
public class LogAspect {
    private static final Logger logger = LoggerFactory.getLogger(LogAspect.class);

    public void beforeStartLog(){
        logger.debug("开始时间为："+System.currentTimeMillis());
    }

    public void afterEndLog(){
        logger.debug("结束时间为："+System.currentTimeMillis());
    }
}
