package org.lx.bird.struct.spring.service.impl;

import org.lx.bird.struct.spring.service.ParseService;

/**
 * json的解析
 * Created by liuxu on 7/20/17.
 */
public class JSONParseService implements ParseService {
    @Override
    public String parseHead(String context) {
        return "{'userName':'zxx','age':20}";
    }
}
