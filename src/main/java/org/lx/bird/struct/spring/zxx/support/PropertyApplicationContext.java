package org.lx.bird.struct.spring.zxx.support;

import org.springframework.context.ApplicationContext;

/**
 * 实现从properties文件当中读取配置信息的context上下文
 * Created by liuxu on 2017-09-08.
 */
public class PropertyApplicationContext extends AbstractPropertyApplicationContext {

    public PropertyApplicationContext(ApplicationContext parent, boolean refresh, String... configurations) {
        super(parent);
        setConfigLocations(configurations);
        if (refresh) {
            refresh();
        }
    }

    public PropertyApplicationContext(String configuration) {
        this(null, true, configuration);
    }

    public PropertyApplicationContext(ApplicationContext parent, boolean refresh, String configComponent) {
        super(parent);
        setConfigLocation(configComponent);
        if (refresh) {
            refresh();
        }
    }
}
