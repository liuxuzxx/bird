package org.lx.bird.struct.springboot.client.remote;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import feign.Response;

/**
 * 压缩流的远程服务
 * Create by xu.liu on 2018-03-21
 */
@FeignClient("lx-server")
public interface ZipRemoteService {

    @GetMapping("/zip/down")
    Response dynamicZipStreamDown();
}
