package org.lx.bird.struct.springboot.client.rest;

import java.util.Date;

import org.lx.bird.struct.springboot.client.dto.HeartDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 关于Heart的restful形式的服务接口
 * Created by liuxu on 2017-12-13.
 */
@RestController
@RequestMapping("/heart")
public class HeartRest {
    private static final String VERSION = "7.0-Beta";

    @GetMapping("/version")
    public String version() {
        return VERSION;
    }

    @GetMapping("/user/{userName}")
    public String userInfo(@PathVariable("userName") String userName){
        return "User name is:"+userName;
    }

    @GetMapping("/user/{userName}/{code}")
    public String userInfo(@PathVariable String userName, @PathVariable("code") String code){
        return "名字是:"+userName+"  code编码是"+code;
    }

    @GetMapping("/date")
    public String date(@RequestParam String data){
        return new Date().toString();
    }

    @PostMapping("")
    public String addHeart(@RequestBody HeartDto heartDto){
        return heartDto.toString();
    }
}
