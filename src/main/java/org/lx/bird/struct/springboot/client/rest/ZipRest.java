package org.lx.bird.struct.springboot.client.rest;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lx.bird.struct.springboot.client.remote.ZipRemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import feign.Response;

/**
 * 一个压缩文件的实验restful请求
 * Create by xu.liu on 2018-03-20
 */
@RestController
@RequestMapping("/zip")
public class ZipRest {

    private ZipRemoteService zipRemoteService;

    @Autowired
    public void setZipRemoteService(ZipRemoteService zipRemoteService){
        Assert.notNull(zipRemoteService,"zipRemoteService can not be null.");
        this.zipRemoteService = zipRemoteService;
    }

    @GetMapping("/down")
    public ResponseEntity<Resource> downZip() throws IOException {
        Response response = zipRemoteService.dynamicZipStreamDown();
        Map<String, Collection<String>> headers = response.headers();
        HttpHeaders httpHeaders = new HttpHeaders();
        headers.forEach((key, values) -> {
            List<String> headerValues = new LinkedList<>();
            headerValues.addAll(values);
            httpHeaders.put(key, headerValues);
        });

        Response.Body body = response.body();
        if(body==null){
            throw new IOException("http body is null or empty.");
        }
        InputStream inputStream = body.asInputStream();
        InputStreamResource resource = new InputStreamResource(inputStream);
        return ResponseEntity
                .status(response.status())
                .headers(httpHeaders)
                .body(resource);
    }

}
