package org.lx.bird.struct.springboot.client.service;

import java.io.OutputStream;

/**
 * 实验压缩流
 * Create by xu.liu on 2018-03-20
 */
public interface ZipService {
    OutputStream fetchDynamicZipStream();
}
