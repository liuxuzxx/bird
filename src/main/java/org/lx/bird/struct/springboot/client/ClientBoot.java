package org.lx.bird.struct.springboot.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * 创建一个实验的spring boot
 * Created by liuxu on 2017-11-10.
 */
@SpringBootApplication(scanBasePackages = {"org.lx.bird.struct.springboot.client.rest"})
@EnableEurekaClient
@EnableFeignClients
public class ClientBoot {

    public static void main(String[] args){
        SpringApplication.run(ClientBoot.class,args);
    }
}
