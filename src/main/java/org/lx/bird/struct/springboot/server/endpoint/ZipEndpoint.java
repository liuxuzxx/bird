package org.lx.bird.struct.springboot.server.endpoint;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.lf5.util.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 压缩的服务端的代码
 * Create by xu.liu on 2018-03-21
 */
@RestController
@RequestMapping("/zip")
public class ZipEndpoint {

    private static Logger logger = LoggerFactory.getLogger(ZipEndpoint.class);

    @GetMapping("/down")
    public void downloadFiles(HttpServletResponse response) {
        response.reset();
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        String downloadName = "test.zip";
        response.setHeader("Content-Disposition", "attachment;fileName=\"" + downloadName + "\"");

        Path path = Paths.get("/home/user/Downloads");
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream())) {
            List<Path> collect = Files.list(path).collect(Collectors.toList());
            zipOutputStream.setMethod(ZipOutputStream.STORED);
            for (Path tempPath : collect) {
                try (InputStream inputStream = Files.newInputStream(tempPath)) {
                    zipOutputStream.putNextEntry(generateZipEntry(tempPath));
                    StreamUtils.copy(inputStream, zipOutputStream);
                }
            }
        } catch (IOException ex) {
            logger.error("Compress file failed...", ex);
        }
    }

    private ZipEntry generateZipEntry(Path path) throws IOException {
        ZipEntry zipEntry = new ZipEntry(path.getFileName().toString());
        zipEntry.setMethod(ZipEntry.STORED);
        zipEntry.setSize(Files.size(path));
        zipEntry.setCrc(getCRC32(path));
        return zipEntry;
    }

    private static long getCRC32(Path path) throws IOException {
        byte[] buffer = new byte[8192];
        CRC32 crc32 = new CRC32();
        try (InputStream fileInputStream = Files.newInputStream(path)) {
            int length;
            while ((length = fileInputStream.read(buffer)) != -1) {
                crc32.update(buffer, 0, length);
            }
            return crc32.getValue();
        } catch (IOException ex) {
            throw new IOException();
        }
    }
}
