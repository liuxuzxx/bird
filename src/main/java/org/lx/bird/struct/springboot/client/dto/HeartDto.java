package org.lx.bird.struct.springboot.client.dto;

import java.util.Date;

/**
 * 一个测试的dto对象
 * Created by liuxu on 2018-01-25.
 */
public class HeartDto {
    private Date createTime;
    private String code;
    private String name;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
