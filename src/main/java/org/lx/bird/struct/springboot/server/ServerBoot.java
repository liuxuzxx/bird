package org.lx.bird.struct.springboot.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * 一个springboot的实验的启动
 * Created by liuxu on 8/1/17.
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class ServerBoot {

    public static void main(String[] args){
        SpringApplication.run(ServerBoot.class,args);
    }
}
