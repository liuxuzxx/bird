package org.lx.bird.struct.activemq;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.lx.bird.struct.activemq.message.DownMessage;

/**
 * The topic sender
 * Created by liuxu on 2017-10-23.
 */
public class TopicSender {
    public static void main(String[] args) throws JMSException {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("admin", "admin", "tcp://localhost:61616");
        factory.setTrustAllPackages(true);
        Connection connection = factory.createConnection();
        connection.start();
        Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

        Topic topic = session.createTopic(MessageName.PERSISTENT_TOPIC_MESSAGE);
        MessageProducer producer = session.createProducer(topic);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        for (int index = 0; index < 20; ++index) {
            producer.send(session.createObjectMessage(new DownMessage()));
        }
        producer.close();
        session.close();
        connection.close();
    }
}
