package org.lx.bird.struct.activemq;

import com.alibaba.fastjson.JSONObject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.lx.bird.struct.activemq.message.DownMessage;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

/**
 * JMS发送消息的服务端
 * Created by liuxu on 2017-10-23.
 */
public class Sender {

    public static void main(String[] args) throws JMSException {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("admin", "admin", "tcp://localhost:61616");
        factory.setTrustAllPackages(true);
        Connection connection = factory.createConnection();
        connection.start();
        Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
        Queue queue = session.createQueue(MessageName.PERSISTENT_QUEUE_MESSAGE);
        MessageProducer producer = session.createProducer(queue);
        producer.setDeliveryMode(DeliveryMode.PERSISTENT);
        for (int index = 0; index < 20; ++index) {
            producer.send(session.createObjectMessage(JSONObject.toJSONString(new DownMessage())));
        }
        producer.close();
        session.close();
        connection.close();
    }

}
