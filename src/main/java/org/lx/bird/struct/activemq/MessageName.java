package org.lx.bird.struct.activemq;

/**
 * 发送的JMS消息的名字
 */
public final class MessageName {
    public static final String PERSISTENT_QUEUE_MESSAGE="persistent.queue.message";
    public static final String NON_PERSISTENT_QUEUE_MESSAGE="non.persistent.queue.message";
    public static final String PERSISTENT_TOPIC_MESSAGE="persistent.topic.message";

    private MessageName(){
        throw new UnsupportedOperationException("不支持创建对象");
    }
}