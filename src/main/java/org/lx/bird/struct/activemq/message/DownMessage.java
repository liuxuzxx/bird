package org.lx.bird.struct.activemq.message;

import java.io.Serializable;
import java.util.UUID;

/**
 * 下载的消息
 * Created by liuxu on 2017-10-23.
 */
public class DownMessage implements Serializable {
    private String uuid;
    private String urlPath;

    public DownMessage() {
        this.uuid = UUID.randomUUID().toString();
        this.urlPath = "http://www.baidu.com";
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }
}
