package org.lx.bird.struct.activemq;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.lx.bird.struct.activemq.message.DownMessage;

import com.alibaba.fastjson.JSONObject;

/**
 * 接受消息
 * Created by liuxu on 2017-10-23.
 */
public class Receiver {

    public static void main(String[] args) throws JMSException {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("admin", "admin", "tcp://localhost:61616");
        factory.setTrustAllPackages(true);
        Connection connection = factory.createConnection();
        connection.start();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue queue = session.createQueue(MessageName.PERSISTENT_QUEUE_MESSAGE);
        MessageConsumer consumer = session.createConsumer(queue);

        consumer.setMessageListener(message -> {
            if (message instanceof ObjectMessage) {
                try {
                    DownMessage downMessage = (DownMessage) ((ObjectMessage) message).getObject();
                    System.out.println(JSONObject.toJSONString(downMessage));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
