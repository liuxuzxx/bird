package org.lx.bird.struct.netty.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author 刘旭
 * @Date 14:03 2018/5/25
 * 创建一个netty的server端
 */
public class NettyServer {
    private static final Logger logger = LoggerFactory.getLogger(NettyServer.class);
    private int port;

    public NettyServer(int port) {
        this.port = port;
    }

    public void init() throws Exception {
        NioEventLoopGroup bossEvents = new NioEventLoopGroup();
        NioEventLoopGroup workerEvents = new NioEventLoopGroup();
        logger.info("Netty服务开始启动在端口：" + port);

        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap = bootstrap.group(bossEvents, workerEvents)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) {
                            socketChannel.pipeline().addLast(new DiscardServerHandler());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            bootstrap.bind(port)
                    .sync()
                    .channel()
                    .closeFuture()
                    .sync();
        } finally {
            workerEvents.shutdownGracefully();
            bossEvents.shutdownGracefully();
        }
    }
}
