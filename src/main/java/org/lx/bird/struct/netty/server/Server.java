package org.lx.bird.struct.netty.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author 刘旭
 * @Date 14:14 2018/5/25
 * Netty的server的执行
 */
public class Server {

    private static final Logger logger = LoggerFactory.getLogger(Server.class);

    public static void main(String[] args) throws Exception {
        new NettyServer(9999).init();
        logger.info("Netty服务器开始运行，提供服务");
    }
}
