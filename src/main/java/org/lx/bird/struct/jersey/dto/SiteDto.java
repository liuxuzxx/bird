package org.lx.bird.struct.jersey.dto;

/**
 * 构造一个站点的信息内容
 * @Author 刘旭
 * @Date 14:19 2018/6/4
 */
public class SiteDto {
    private String name;
    private String url;
    private long startTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
