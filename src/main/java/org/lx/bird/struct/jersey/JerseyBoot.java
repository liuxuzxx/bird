package org.lx.bird.struct.jersey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 使用SpringBoot+Jersey的形式构建RESTFul的服务
 *
 * @Author 刘旭
 * @Date 13:53 2018/6/4
 */
@SpringBootApplication
public class JerseyBoot {
    public static void main(String[] args) {
        SpringApplication.run(JerseyBoot.class, args);
    }
}
