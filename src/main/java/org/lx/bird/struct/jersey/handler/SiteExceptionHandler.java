package org.lx.bird.struct.jersey.handler;

import java.io.FileNotFoundException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 加入一个站点异常的拦截器
 *
 * @Author 刘旭
 * @Date 14:56 2018/6/4
 */
@Provider
public class SiteExceptionHandler implements ExceptionMapper<FileNotFoundException> {
    private static final Logger logger = LoggerFactory.getLogger(SiteExceptionHandler.class);

    public SiteExceptionHandler(){
        logger.info("SiteExceptionHandler被扫描到了");
    }

    @Override
    public Response toResponse(FileNotFoundException exception) {
        logger.error(exception.getMessage(),exception);
        logger.error("拦截到一个异常信息内容");
        return Response.status(Response.Status.BAD_REQUEST).entity("请求数据不对，打回")
                .type(MediaType.APPLICATION_JSON_TYPE).build();
    }
}
