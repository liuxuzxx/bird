package org.lx.bird.struct.jersey.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Jersey(球衣)的配置
 * @Author 刘旭
 * @Date 13:53 2018/6/4
 */
@Component
public class JerseyConfig extends ResourceConfig {
    private static final Logger logger = LoggerFactory.getLogger(JerseyConfig.class);

    public JerseyConfig(){
        logger.info("Jersey的托管类加载package的信息，和扫描");
        packages("org.lx.bird.struct.jersey");
    }
}
