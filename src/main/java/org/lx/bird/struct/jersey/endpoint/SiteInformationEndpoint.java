package org.lx.bird.struct.jersey.endpoint;

import java.io.FileNotFoundException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.lx.bird.struct.jersey.dto.SiteDto;
import org.springframework.stereotype.Component;

/**
 * 站点信息的endpoint
 * @Author 刘旭
 * @Date 13:56 2018/6/4
 */
@Component
@Path("/site")
public class SiteInformationEndpoint {
    private static final SiteDto CONSTANT_SITE = new SiteDto();

    static{
        CONSTANT_SITE.setName("SpringBoot-Jersey");
        CONSTANT_SITE.setUrl("http://localhost:8080/");
        CONSTANT_SITE.setStartTime(System.currentTimeMillis());
    }

    @GET
    @Path("/site-name")
    @Produces(MediaType.APPLICATION_JSON)
    public SiteDto siteName(){
        return CONSTANT_SITE;
    }

    @GET
    @Path("/exception")
    public String siteException() throws FileNotFoundException {
        throw new FileNotFoundException("尝试着抛出一个异常信息，查看Jersey如何拦截");
    }
}
