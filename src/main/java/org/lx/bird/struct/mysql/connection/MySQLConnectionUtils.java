package org.lx.bird.struct.mysql.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 获取MySQL连接的工具类，我想使用最原始的方式获取数据库连接，然后测试性能
 * Created by liuxu on 18-5-21.
 */
public class MySQLConnectionUtils {
    private static final Logger logger = LoggerFactory.getLogger(MySQLConnectionUtils.class);

    private static final String URL = "jdbc:mysql://localhost:3306/share?useUnicode=true&characterEncoding=UTF8";
    private static final String USER_NAME = "root";
    private static final String PASSWORD = "root";
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";

    public static Connection fetchConnection() {
        try {
            Class.forName(DRIVER);
            return DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        } catch (ClassNotFoundException e) {
            logger.error("找不到JDBC驱动器类", e);
        } catch (SQLException e) {
            logger.error("获取数据库连接出现错误.", e);
        }
        throw new RuntimeException("获取数据库连接失败");
    }
}
