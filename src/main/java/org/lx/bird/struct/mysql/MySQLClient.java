package org.lx.bird.struct.mysql;

import org.junit.Test;
import org.lx.bird.struct.mysql.connection.MySQLConnectionUtils;
import org.lx.bird.struct.mysql.select.EmbedSelect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 执行MySQL的客户端，就是验证《高性能MySQL》这本书的一些实验工作
 * Created by liuxu on 18-5-21.
 */
public class MySQLClient {
    private static Connection connection = MySQLConnectionUtils.fetchConnection();
    private static final Logger logger = LoggerFactory.getLogger(MySQLClient.class);

    @Test
    public void embedExistsSql() throws SQLException {
        long start = System.currentTimeMillis();
        EmbedSelect embed = new EmbedSelect(connection);
        embed.existsSql();
        long end = System.currentTimeMillis();
        logger.info("Exists语句的执行时间为：" + (end - start) / 1000.0 + " 秒钟");
    }
}
