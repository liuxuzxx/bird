package org.lx.bird.struct.mysql.select;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 测试嵌入形式的sql查询的时间性能
 * Created by liuxu on 18-5-21.
 */
public class EmbedSelect {

    private Connection connection;

    public EmbedSelect(Connection connection) {
        this.connection = connection;
    }

    public void existsSql() throws SQLException {
        String existsSql = "select * from share_info where exists (select * from share where share.share_code=share_info.share_code and share.share_code='601518')";
        Statement statement = connection.createStatement();
        boolean flag = statement.execute(existsSql);
    }
}
