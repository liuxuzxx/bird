package org.lx.bird.struct.lucene.dto;

/**
 * The lucene search article dto
 * Created by liuxu on 7/13/17.
 */
public class ArticleDTO {
    private Integer id;
    private String title;
    private String content;
    private String author;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
