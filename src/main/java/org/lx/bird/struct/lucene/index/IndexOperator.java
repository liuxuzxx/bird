package org.lx.bird.struct.lucene.index;

import com.alibaba.fastjson.JSONObject;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;
import org.lx.bird.struct.lucene.dto.ArticleDTO;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * The lucene index operation
 * Created by liuxu on 7/13/17.
 */
public class IndexOperator {

    public static final String INDEX_DIR = "./indexDir/";

    @Test
    public void createIndex() throws IOException {
        ArticleDTO article = new ArticleDTO();
        article.setId(12306);
        article.setAuthor("王垠");
        article.setTitle("only use linux");
        article.setContent("完全用Linux工作，抛弃windows.");
        constructDocument(article);

        article.setId(10000);
        article.setAuthor("Garnet Kiv");
        article.setTitle("Mybatis can only do this");
        article.setContent("MyBatis是一种半自动形式的和database进行映射的中间件");
        constructDocument(article);
    }

    private void constructDocument(ArticleDTO article) throws IOException {
        Document document = new Document();
        document.add(new LongPoint("id", article.getId()));
        document.add(new StringField("author", article.getAuthor(), Field.Store.YES));
        document.add(new TextField("title", article.getTitle(), Field.Store.YES));
        document.add(new TextField("content", article.getContent(), Field.Store.YES));
        FSDirectory indexDirs = FSDirectory.open(Paths.get(INDEX_DIR));
        Analyzer standardAnalyzer = new StandardAnalyzer();
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(standardAnalyzer);
        IndexWriter indexWriter = new IndexWriter(indexDirs, indexWriterConfig);
        indexWriter.addDocument(document);
        indexWriter.close();
    }

    @Test
    public void search() throws IOException, ParseException {
        String queryString = "only";

        Directory directory = FSDirectory.open(Paths.get(INDEX_DIR));
        Analyzer analyzer = new StandardAnalyzer();

        QueryParser queryParser = new QueryParser("title", analyzer);
        Query query = queryParser.parse(queryString);

        IndexReader indexReader = DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        TopDocs topDocs = indexSearcher.search(query, 2);
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;

        List<ArticleDTO> articleList = new ArrayList<>();
        for (ScoreDoc scoreDoc : scoreDocs) {
            int docId = scoreDoc.doc;
            Document doc = indexSearcher.doc(docId);
            ArticleDTO article = new ArticleDTO();
            article.setTitle(doc.getField("title").stringValue());
            article.setAuthor(doc.getField("author").stringValue());
            articleList.add(article);
        }

        indexReader.close();

        System.out.println("总结果数量为:" + articleList.size());
        System.out.println(JSONObject.toJSONString(articleList));
    }
}
