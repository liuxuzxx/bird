package org.lx.bird.struct.algorithm.tree;

import org.lx.bird.javacore.dto.UserDto;
import org.lx.bird.struct.mybatis.dto.UserDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * 创建一个二叉搜索树，包含堆树的增删改查的操作
 * Created by liuxu on 17-8-26.
 */
public class BinarySearchTree {

    private TreeNode root;

    public TreeNode getRoot() {
        return this.root;
    }

    public BinarySearchTree(UserDto userDto) {
        this.root = new TreeNode(userDto);
    }

    public void addNote(UserDto userDto) {
        TreeNode search = root;
        while (search != null) {
            if (search.getSelf().getUserId() > userDto.getUserId()) {
                if (search.getLeft() == null) {
                    break;
                }
                search = search.getLeft();
            } else {
                if (search.getRight() == null) {
                    break;
                }
                search = search.getRight();
            }
        }
        TreeNode insertNode = new TreeNode(userDto);
        if (search.getSelf().getUserId() > userDto.getUserId()) {
            search.setLeft(insertNode);
        } else {
            search.setRight(insertNode);
        }
        insertNode.setParent(search);
    }

    /**
     * 进行一个排序的输出二叉搜索树的信息
     */
    public void sort(){
        middleSort(getRoot());
    }
    private void middleSort(TreeNode root){
        if(root!=null){
            middleSort(root.getLeft());
            System.out.println(root.getSelf().getUserId()+" "+root.getSelf().getUserName());
            middleSort(root.getRight());
        }
    }

    /**
     * 主要是打印出来这个二叉搜索树的一个顺序的信息，这样子，便于我们
     * 进行一个错误的查找过程的出现问题
     *
     * @return string  tree的二叉搜索的一个排序之后的字符串信息
     */
    @Override
    public String toString() {
        return null;
    }

    class TreeNode {
        private TreeNode parent;
        private TreeNode left;
        private TreeNode right;

        private UserDto self;

        public TreeNode(UserDto self) {
            this.self = self;
        }

        public UserDto getSelf() {
            return this.self;
        }

        public TreeNode getParent() {
            return parent;
        }

        public void setParent(TreeNode parent) {
            this.parent = parent;
        }

        public TreeNode getLeft() {
            return left;
        }

        public void setLeft(TreeNode left) {
            this.left = left;
        }

        public TreeNode getRight() {
            return right;
        }

        public void setRight(TreeNode right) {
            this.right = right;
        }
    }
}