package org.lx.bird.struct.algorithm.tree;

import com.alibaba.fastjson.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.lx.bird.javacore.dto.UserDto;

import javax.annotation.PostConstruct;
import java.util.Date;

/**
 * 测试我们的搜索二叉树
 * Created by liuxu on 17-8-26.
 */
public class BinarySearchTreeTest {

    private static BinarySearchTree tree = null;

    @BeforeClass
    public static void init() {
        UserDto userDto = new UserDto();
        userDto.setBirthday(new Date());
        userDto.setUserId(200);
        userDto.setUserName("index:" + userDto.getUserId());

        tree = new BinarySearchTree(userDto);
    }

    /**
     * 测试产生一个root节点的树
     */
    @Test
    public void createTree() {
        BinarySearchTree.TreeNode root = tree.getRoot();
        System.out.println(JSONObject.toJSONString(root));
    }

    /**
     * 添加节点的测试
     */
    @Test
    public void addNode(){
        UserDto node = new UserDto();
        node.setUserId(128);
        node.setBirthday(new Date());
        node.setUserName("admin");

        tree.addNote(node);
        tree.addNote(buildUser(369));
        tree.addNote(buildUser(12));
        System.out.println(tree.getRoot());
        tree.sort();
    }

    private UserDto buildUser(int userId){
        UserDto userDto = new UserDto();
        userDto.setUserId(userId);
        userDto.setUserName("index:"+userId);
        userDto.setBirthday(new Date());
        return userDto;
    }
}
